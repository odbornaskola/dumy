<?php

define('SHELL_INSTALL', true);

// Should be run as whoever is running the webserver
if (chdir(pathinfo($argv[0], PATHINFO_DIRNAME) . '/../www-root')) {
    require 'autoload.php';

    $shell_manifest = SHELL_PATH . '/../install/manifest.php';
    require_once $shell_manifest;

    $app_manifest = PROJECT_PATH . '/../install/manifest.php';
    if (file_exists($app_manifest)) {
        require_once $app_manifest;
    }

    \Sh\Ell\Shell::install();
    \Sh\Ell\Shell::end();
}





