<?php

if (!defined('SHELL_INIT')) {
    if (PHP_SAPI === 'cli') {
        if (!defined('SHELL_INSTALL')) {
            chdir(pathinfo(__FILE__, PATHINFO_DIRNAME) . '/../www-root');
        }
        $path = getcwd();
        $cfg = loadCfgLocal('../config/local.inc.php');
    }
    else {
        $cfg = loadCfgLocal('../config/local.inc.php');
        $path = rtrim($cfg->document_root != '' ? $cfg->document_root : $_SERVER['DOCUMENT_ROOT'], '/');
        if (strlen($cfg->relroot)) {
            $path.= '/' . trim($cfg->relroot, '/');
        }
    }

    define('PROJECT_NAME', $cfg->appname);
    // disctinct the install script and web run
    if (!defined('PROJECT_PATH')) {
        define('PROJECT_PATH', realpath($path));
    }

    define('PROJECT_CLASS_DIR', realpath(PROJECT_PATH . '/../lib/'));
    set_include_path(PROJECT_CLASS_DIR . PATH_SEPARATOR . get_include_path());

    if (PROJECT_NAME != '') {
        define('SHELL_PATH', realpath(PROJECT_PATH . '/../../shell/www-root'));
        define('SHELL_CLASS_DIR', realpath(SHELL_PATH . '/../lib/'));
        set_include_path(SHELL_CLASS_DIR . PATH_SEPARATOR . get_include_path());
    }
    else {
        define('SHELL_PATH', PROJECT_PATH);
        define('SHELL_CLASS_DIR', PROJECT_CLASS_DIR);
    }

    //spl_autoload_extensions('.php');
    spl_autoload_register();

    define('SHELL_INIT', true);
}

/**
 * @param string $cfgFile
 * @return \ConfigLocal
 */
function loadCfgLocal($cfgFile) {
    if (!file_exists($cfgFile)) {
        die('ConfigLocal: ' . $cfgFile . ' not found!');
    }

    require_once $cfgFile;

    return new \ConfigLocal;
}
