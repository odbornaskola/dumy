<?php

// cron má trochu jiný start, nejprve se zkusí načíst nastavení, aby se mohlo
// načíst nastavení cronu, které rozhodne, zda-li se vůbec cron spustí
$cfgfile = '../config/local.inc.php';

if (!file_exists($cfgfile)) {
    die($cfgfile . ' not found!');
}

require_once $cfgfile;

$cfg = new \ConfigLocal;


$run = false;

if (PHP_SAPI === 'cli') {
    $run = true;
}
else {
    //FIXME: consider sha ...
    if (isset($_GET['token']) and $_GET['token'] == $cfg->cronToken) {
        if (!isset($_GET['handler'])) {
            $_GET['handler'] = 'Sh/App/Cron';
        }
        $run = true;
    }
}

if ($run) {
    // autoload classes
    require_once 'autoload.php';

    new \Sh\App\Cron();

    \Sh\Ell\Shell::run();

    echo 'DONE';
}
else {
    echo 'ERROR';
}

