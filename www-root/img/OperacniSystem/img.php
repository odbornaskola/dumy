<?php

$dirs = scandir('./');

foreach ($dirs as $d) {
    if ($d != '.' and $d != '..') {
        list($cesky, $anglicky) = explode('_', pathinfo($d, PATHINFO_FILENAME));
        $cesky = str_replace('-', ' ', $cesky);
        $anglicky = str_replace('-', ' ', $anglicky);
        echo '<li>'."\n".
             '    <p>'.$cesky.' ('.$anglicky.')</p>'."\n".
             '    <p><img src="{prefix}/img/{theme}/'.$d.'" alt="'.pathinfo($d, PATHINFO_FILENAME).'"></p>'."\n".
             '</li>'."\n";
    }
}
