CREATE TABLE IF NOT EXISTS `lednicka` (
  `id` int(11) NOT NULL,
  `polozka` varchar(128) NOT NULL,
  `datum` datetime NOT NULL
);

INSERT INTO `lednicka` (`polozka`, `datum`) 
VALUES 
  ('Mléko', NOW()), 
  ('Máslo', NOW()),
  ('Salám', NOW()),
  ('Sirup', NOW()),
  ('Kečup', NOW());