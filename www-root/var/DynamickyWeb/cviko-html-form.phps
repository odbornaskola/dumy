<form>
    <h2>Contactel formium</h2>
    <table>
        <tr>
            <td><label for="i-name">Jméno</label></td>
            <td><input id="i-name" type="text" name="name" value=""></td>
        </tr>
        <tr>
            <td><label for="i-email">E-mail</label></td>
            <td><input id="i-email" type="text" name="email" value=""></td>
        </tr>
        <tr>
            <td><label for="i-immodium">Immodium</label></td>
            <td>
                <select id="i-immodium" name="immodium" size="1">
                    <option value="">tellus</option>
                    <option value="">dignissim</option>
                    <option value="">libero</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><label for="i-duis">Duis</label></td>
            <td><input id="i-duis" type="checkbox" name="duis" value=""> (convallis nec nunc)</td>
        </tr>
        <tr>
            <td><label for="i-zprava">Zpráva</label></td>
            <td><textarea id="i-zprava" name="zprava" cols="40" rows="12"></textarea></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: right">
                <input type="submit" value="Dolor">
            </td>
        </tr>
    </table>
</form>