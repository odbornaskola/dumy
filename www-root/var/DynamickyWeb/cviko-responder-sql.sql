CREATE TABLE `responder` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  `immodium` varchar(32) NOT NULL,
  `duis` tinyint NOT NULL DEFAULT '0',
  `zprava` text NOT NULL
);