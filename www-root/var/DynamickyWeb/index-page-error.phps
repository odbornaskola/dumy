<?php

$title = "Lorem ipsum";

include "header.php";

if (isset($_GET["page"])) {
    $pozadavek = $_GET["page"];
}
else {
    $pozadavek = "uvod";
}

switch ($pozadavek) {
    case "uvod":
        $page = "uvod.php";
        break;
    case "kontakty":
        $page = "kontakty.php";
        break;
    default:
        $page = "error.php";
        break;
}

include $page;

include "footer.php";