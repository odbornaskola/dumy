<?php
switch ($_GET["page"]) {
    case "domu":
        include "pages/domu.php";
        break;
    case "katalog":
        include "pages/katalog.php";
        break;
    case "objednavka":
        include "pages/objednavka.php";
        break;
    case "kontakt":
        include "pages/kontakt.php";
        break;
}

/*
 * Všimněte si, že v tomto případě jsou všechny vkládané stránky navíc v podadresáři "pages".
 */