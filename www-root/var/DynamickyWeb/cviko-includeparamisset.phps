<?php
if (isset($_GET["page"])) {
    switch ($_GET["page"]) {
        case "domu":
            include "pages/domu.php";
            break;
        case "katalog":
            include "pages/katalog.php";
            break;
        case "objednavka":
            include "pages/objednavka.php";
            break;
        case "kontakt":
            include "pages/kontakt.php";
            break;
    }
}