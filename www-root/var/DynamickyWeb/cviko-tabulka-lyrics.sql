CREATE TABLE `lyrics` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nazev` varchar(128) NOT NULL,
  `album` varchar(128) NOT NULL,
  `delka` time NOT NULL,
  `autor` varchar(128) NOT NULL,
  `text_orig` text NOT NULL,
  `text_cs` text NOT NULL
);