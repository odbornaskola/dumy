public static void Main(string[] args)
{
    Console.WriteLine(ZadejCislo("Zadej libovolné číslo: "));

    Console.ReadKey(false);
}

public static double ZadejCislo(string vyzva) {
    Console.Write(vyzva);
    double cislo = double.Parse(Console.ReadLine());

    return cislo;
}