class Program
{
	public static string abeceda = "aábcčdďeéěfghiíjklmnňoópqrřsštťuúůvwxyýzž ,.:?!";
	public static int klic = 3;
	
	public static void Main(string[] args)
	{
		
		string sifra = Sifruj("ahoj světe");
		Console.WriteLine(sifra);
		
		string text = DeSifruj("cjqm");
		Console.WriteLine(text);
		
		Console.ReadKey(true);
	}
	
	public static string Sifruj(string text) {
		string sifra = "";
		
		int pozice;
		for (int i = 0; i < text.Length; i++) {
			pozice = abeceda.IndexOf(text[i]);
			if (pozice == -1) {
				Console.WriteLine("Znak " + text[i] + " není v abecedě!");
			}
			pozice = pozice + klic;
			if (pozice >= abeceda.Length) {
				pozice = pozice - abeceda.Length;
			}
			sifra = sifra + abeceda[pozice];
		}
		
		return sifra;
	}
	
	public static string DeSifruj(string sifra) {
		string text = "";
		
		int pozice;
		for (int i = 0; i < sifra.Length; i++) {
			pozice = abeceda.IndexOf(sifra[i]);
			if (pozice == -1) {
				Console.WriteLine("Znak " + sifra[i] + " není v abecedě!");
			}
			pozice = pozice - klic;
			if (pozice < 0) {
				pozice = pozice + abeceda.Length;
			}
			text = text + abeceda[pozice];
		}
		
		return text;
	}
}
