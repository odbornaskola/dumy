public static void Main(string[] args)
{
    Console.WriteLine("Obsah čtverce: " + ObsahCtverce(ZadejCislo("Zadej stranu a: ")) + " j^2");

    Console.ReadKey(false);
}

public static double ZadejCislo(string vyzva) {
    Console.Write(vyzva);
    double cislo = double.Parse(Console.ReadLine());

    return cislo;
}

public static double ObsahCtverce(double a) {
    double obsah;
    obsah = a*a;

    return obsah;
}