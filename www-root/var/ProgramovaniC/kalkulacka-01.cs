using System;

namespace Kalkulacka1
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            Console.WriteLine("Program: Kalkulačka v1.0\n");
            // "\n" je sekvence pro klávesu Enter - provede odřádkování
         
            /**
             * Nechejte uživatele po sobě zadat dvě celá čísla.
             *   Console.ReadLine()
             * Následně tato čísla sečtěte.
             *
             * Výsledek vypište na obrazovku.
             *   Console.WriteLine("...")
             *
             * Rozšiřte program o: *, /, -
             */
            
            // 1. deklarace prom.
            int x;
            int y;
            int vysK;
            int vysP;
            int vysM;
            int vysD;

            // 2. přiřazení hodnot
            Console.Write("Zadej číslo: ");
            x = int.Parse(Console.ReadLine());
            
            Console.Write("Zadej další číslo: ");
            y = int.Parse(Console.ReadLine());

            // 3. operace
            vysP = x + y;
            vysM = x - y;
            vysK = x * y;
            vysD = x / y;

            // 4. výstup
            Console.WriteLine(x + " + " + y + " = " + vysP);
            Console.WriteLine(x + " - " + y + " = " + vysM);
            Console.WriteLine(x + " * " + y + " = " + vysK);
            Console.WriteLine(x + " / " + y + " = " + vysD);
            
            Console.WriteLine("\n");
            
            Console.Write("Press any key to continue . . . ");
            Console.ReadKey(true);
        }
    }
}

