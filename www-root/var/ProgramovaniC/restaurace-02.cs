using System;

namespace Restaurace2
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            Console.WriteLine("Program: Restaurace v2.0");
         
            /**
             * Vytvořte program, který
             * - vyzve uživatele k zadání názvu jídla
             * - vyzve uživatele k zadání ceny jídla bez DPH
             * - vyzve k zadání DPH
             *
             * - následně spočítá cenu jídla včetně DPH
             * - zobrazí podrobný výpis
             *   Jídlo        : Palačinky
             *   Cena bez DPH : 4.50
             *   DPH          : 21%
             *   ...
             */

            // 1. deklarace prom.
            string nazev;
            double cena; // cena jídla bez dph
            double dph; // dph
            double celkem; // celková cena
            
            // 2. vstup
            Console.Write("Zadej název jídla: ");
            nazev = Console.ReadLine();
            
            Console.Write("Zadej cenu jídla: ");
            cena = double.Parse(Console.ReadLine());
            
            Console.Write("Zadej DPH (v %): ");
            dph = double.Parse(Console.ReadLine());
            dph = dph / 100.0;
            // 3. operace
            celkem = cena * dph + cena;

            // 4. výpis
            Console.WriteLine("Jídlo        : " + nazev);
            Console.WriteLine("DPH          : " + (dph * 100) + "%");
            Console.WriteLine("Celkem       : " + celkem);

            Console.Write("Press any key to continue . . . ");
            Console.ReadKey(true);
        }
    }
}

