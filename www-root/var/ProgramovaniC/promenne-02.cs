using System;

namespace Promenne02
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            /*
             * Cvičení I
             *
             * Do proměnných "x" a "y" vložte lib. celá čísla.
             * Do proměnné "z" uložte součet "x" a "y".
             * Vypište proměnnou "z"
             *
             */

            // 1. deklarece prom.
            int x;
            int y;
            int z;

            // 2. přiřazení hodn.
            x = 12;
            y = 30;

            // 3. operace
            z = x + y;

            // 4. výpis (výstup)
            Console.WriteLine("Výsledek = " + z);

            /*
             * Cvičení II
             *
             * Do proměnné "a" celočíselného typu uložte lib. číslo.
             * Do proměnné "b" desetinného typu uložte lib. desetinné číslo.
             * Do proměnné "c" uložte součet proměnných a+b
             * Vypište proměnnou "c"
             */

            int a = 3;
            double b = 3.14;
            // int c; nelze použít, protože výsledný datový typ musí být stejný nebo vyšší než ten, který může objevit na výstupu operace
            // je to stejné jako s množinami čísel, desetinná čísla mohou obsahovat i celá čísla, ale u množiny celých čísel nemohou být čísla desetinná
            double c;

            // sečíst a+b a uložit do "c"
            // vypsat "c"

            c = a + b;

            Console.WriteLine("Výsledek = " + c);


            Console.Write("Press any key to continue . . . ");
            Console.ReadKey(true);
        }
    }
}
