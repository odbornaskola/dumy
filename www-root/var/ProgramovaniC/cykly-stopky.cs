int milis = 0;
int sec = 0;
int min = 0;
int hod = 0;

Console.WriteLine("Stiskni mezerník pro start/konec");
Console.ReadKey(true);

while (true) {
	if (milis == 1000) {
		sec++;
		milis = 0;
	}
	
	if (sec == 60) {
		min++;
		sec = 0;
	}
	
	if (min == 60) {
		hod++;
		min = 0;
	}
	
	if (hod == 24) {
		break;
	}
	
	if (Console.KeyAvailable) {
		if (Console.ReadKey(true).Key == ConsoleKey.Spacebar) {
			break;
		}
	}
	
	Console.SetCursorPosition(0, 2);
	Console.Write("{0}:{1}:{2}.{3}     ", hod, min, sec, milis);
	
	Thread.Sleep(100);
	
	milis+= 100;
}
