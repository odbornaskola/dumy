using System;

namespace Restaurace
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            /**
             * Vytvořte program, který si bude pamatovat název a cenu jídla.
             * Přičemž cena jídla je bez DPH.
             * Dále bude mít program uloženou hodnotu DPH.
             *
             * Program bude počítat cenu jídla včetně DPH.
             *
             * Nakonec zahrňte do výpočtu spropitné 10% z ceny jídla (včetně DPH).
             *
             * Vytvořte podrobný výstup kalkulace.
             */

            Console.WriteLine("Program: Restaurace\n");

            string nazev; // název jídla
            double jidlo; // cena jídla v €
            double dph;
            double cena;
            double tip = 0.10;
            double cena_s_dph;

            nazev = "Palačinky";
            jidlo = 4.50;
            dph   = 0.20; // 20%

            // cena jídla + dph
            cena_s_dph = jidlo * dph + jidlo;
            // cena jídla + tip
            cena = cena_s_dph * tip + cena_s_dph;

            Console.WriteLine("Jídlo       : " + nazev);
            Console.WriteLine("Cena bez DPH: " + jidlo);
            Console.WriteLine("DPH         : " + (dph*100) + "%");
            Console.WriteLine("Spropitné   : " + (cena_s_dph*tip) + " euro");
            Console.WriteLine("Celkem      : " + cena + " euro");
            Console.WriteLine("\n");

            Console.Write("Press any key to continue . . . ");
            Console.ReadKey(true);
        }
    }
}
