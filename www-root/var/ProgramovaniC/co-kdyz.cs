int x;

Console.Write("Zadej celé číslo: ");
x = int.Parse(Console.ReadLine());

if (x > 0) {
    Console.WriteLine("Číslo je kladné");
}

if (x < 0) {
    Console.WriteLine("Číslo je záporné");
}

if (x == 0) {
    Console.WriteLine("Číslo je nula");
}
