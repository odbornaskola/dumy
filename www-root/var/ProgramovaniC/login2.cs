public static void Main(string[] args)
{
    string jmeno = ZadejJmeno();
    string heslo = ZadejHeslo();

    if (Prihlaseni(jmeno, heslo) == true) {
        Console.WriteLine("Přihlášení bylo úspěšné.");
    }
    else {
        Console.WriteLine("Přihlášení selhalo.");
    }

    Console.ReadKey(false);
}