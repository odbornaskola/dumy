using System;

namespace Kalkulacka3
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            Console.WriteLine("Program: Kalkulačka v3.0\n");

            /**
             * Vytvořte program podobný Kalkulačce v2.0, ale
             * na místo konstruktu switch použijte if-else
             *
             * V programu bude navíc ošetřeno dělení nulou.
             *
             */

            double x;
            double y;
            double vysledek;
            string op;

            Console.Write("Zadej číslo: ");
            x = double.Parse(Console.ReadLine());
            
            Console.Write("Zadej číslo: ");
            y = double.Parse(Console.ReadLine());
            
            Console.Write("Zadej operaci (*,/,+,-): ");
            op = Console.ReadLine();
            
            if (op == "+") {
                vysledek = x + y;
                Console.WriteLine(x + " " + op + " " + y + " = " + vysledek);
                //Console.WriteLine("{0} {1} {2} = {3}", x, op, y, vysledek);
            }
            else if (op == "-") {
                vysledek = x - y;
                Console.WriteLine(x + " " + op + " " + y + " = " + vysledek);
            }
            else if (op == "*") {
                vysledek = x * y;
                Console.WriteLine(x + " " + op + " " + y + " = " + vysledek);
            }
            else if (op == "/") {
                if (y == 0) {
                    Console.WriteLine("Zadané číslo nemůže být rovno 0.");
                }
                else {
                    vysledek = x / y;
                    Console.WriteLine(x + " " + op + " " + y + " = " + vysledek);   
                }
            }
            else {
                Console.WriteLine("Neznámý operátor!");
            }

            Console.Write("Press any key to continue . . . ");
            Console.ReadKey(true);
        }
    }
}

