Console.WriteLine("Program: Login v1.1\n");

string secretName = "root";
string secretPass = "toor";

string loginName;
string loginPass;

Console.WriteLine("Přihlášení");
Console.Write("login: ");
loginName = Console.ReadLine();

// příprava na změnu barvy
ConsoleColor barva;
barva = Console.ForegroundColor;

Console.Write("heslo: ");
// záměna barvy
Console.ForegroundColor = Console.BackgroundColor;
// načtení hesla
loginPass = Console.ReadLine();
// barva zpět
Console.ForegroundColor = barva;

// autentizace
if (secretName == loginName && secretPass == loginPass) {
    Console.WriteLine("\nPřihlášení proběhlo úspěšně");
}
else {
    Console.WriteLine("\nChybně zadané jméno nebo heslo!");
}
