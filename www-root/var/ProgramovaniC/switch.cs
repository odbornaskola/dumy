using System;

namespace Prepinac
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            Console.WriteLine("Program: Přepínač v1.0\n");

            string volba;
            Console.Write("Zvol buď \"a\" nebo \"b\": ");
            volba = Console.ReadLine();

            switch (volba) {
            case "a":
                Console.WriteLine("Áčko");
                break;

            case "b":
                Console.WriteLine("Béčko");
                break;

            default:
                 Console.WriteLine("Neznám");
                 break;
            }

            Console.Write("Press any key to continue . . . ");
            Console.ReadKey(true);
        }
    }
}
