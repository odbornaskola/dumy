using System;

namespace Test
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            Console.WriteLine ("Program: Download v1.0\n");

            /**
             * Vytvořte program pro výpočet doby stahování dat.
             *
             * Do proměnné uložte objem přenášených dat v MB (např. 1024 MB)
             * Do jiné proměnné uložte rychlost stahování v Mbit/s (např. 10 Mbit/s)
             *
             * Hlavní zádrhel programu bude nejspíše v tom,
             * že velikost se uvádí v bytech a rychlost bitech za vteřinu.
             *
             * Aby bylo možné spočítat dobu stahování je potřeba:
             *   - převést MB na Mbit (1B = 8bit, takže 1024*8)
             *   - provést podíl velikosti dat a rychlosti stahování (zde 8192/10)
             *
             * Vypište výsledek na monitor.
             * Přepočítejte čas ve vteřinách na minuty.
             */


            // deklarace
            double objem;
            double objemBit;
            double rychlost;
            double cas;

            // přiřazení hodnot
            objem = 1024; // velikost v MB
            rychlost = 10; // rychlost v Mbit/s

            // operace
            objemBit = objem * 8;
            cas = objemBit / rychlost;

            // výstup
            Console.WriteLine("Objem dat        : " + objem + " MB");
            Console.WriteLine("Rychlost přenosu : " + rychlost + " Mbit/s");
            Console.WriteLine("Převod MB->Mbit  : " + objemBit + " Mbit");
            Console.WriteLine("-----------------------------");
            Console.WriteLine("Doba stahování   : " + cas + " s");
            Console.WriteLine("                   " + (cas/60) + " m");
        }
    }
}
