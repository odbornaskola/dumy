while (true) {
	Console.WriteLine("true");
}

while (false == false) {
	Console.WriteLine("false == false");
}

while (1 > 0.1) {
	Console.WriteLine("1 > 0.1");
}

while ("lorem" == "lorem") {
	Console.WriteLine("lorem == lorem");
}

string t1 = "lorem";
string t2 = "ipsum";
while (t1 != t2) {
	Console.WriteLine("t1 != t2");
}
