Console.WriteLine ("Mysli si číslo v rozsahu 1..100");

int min = 1;
int max = 100;
int odhad = 0;
string volba = "";

while (volba != "trefa") {
    odhad = ((max - min) / 2) + min;
    Console.Write("Je tvé číslo větší,menší,rovno číslu {0}: ", odhad);
    volba = Console.ReadLine().ToLower();
    if (volba == "větší" || volba == "v" || volba == "vetsi") {
        min = odhad + 1;
    }
    else if (volba == "menší" || volba == "m" || volba == "mensi") {
        max = odhad - 1;
    }
    else if (volba == "rovno" || volba == "r") {
        Console.WriteLine("Trefa!");
        break;
    }

    if (min > max) {
        Console.WriteLine("Nepodvádět!");
        break;
    }
}
