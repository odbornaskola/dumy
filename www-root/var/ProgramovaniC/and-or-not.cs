Console.WriteLine ("Program: and-or-not v1.0");

string a;
string b;

Console.Write("Zadej log. 1 nebo 0: ");
a = Console.ReadLine();
Console.Write("Zadej ještě jednu log. 1 nebo 0: ");
b = Console.ReadLine();

Console.WriteLine("Logický součin - &&");
if (a == "1" && b == "1") {
    Console.WriteLine("{0} && {1} = 1", a, b);
}
else {
    Console.WriteLine("{0} && {1} = 0", a, b);
}

Console.WriteLine("Logický součet - ||");
if (a == "0" && b == "0") {
    Console.WriteLine("{0} || {1} = 0", a, b);
}
else {
    Console.WriteLine("{0} || {1} = 1", a, b);
}

Console.WriteLine("Logická negace - &&");
if (a == "1") {
    Console.WriteLine("{0} ! 0", a);
}
else {
    Console.WriteLine("{0} ! 1", a);
}
if (b == "1") {
    Console.WriteLine("{0} ! 0", b);
}
else {
    Console.WriteLine("{0} ! 1", b);
}
