string volba;

Console.Write("Jsi muž nebo žena? Odpověz \"muž\" nebo \"žena\": ");
volba = Console.ReadLine();

if (volba == "muž") {
    Console.WriteLine("Eviduji muže.");
}
else if (volba == "žena") {
    Console.WriteLine("Eviduji ženu.");
}
else {
    Console.WriteLine("Chyba zadání.");
}
