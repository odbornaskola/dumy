// generování náhodného čísla
Random nahoda = new Random();
int cislo = nahoda.Next(1, 8);

// nazev souboru s hádankou
string hadankaSoubor = "hadanka-" + cislo + ".txt";
// nazev souboru s hádankou
string odpovedSoubor = "odpoved-" + cislo + ".txt";

// test a otevření souboru
if (File.Exists(hadankaSoubor) && File.Exists(odpovedSoubor)) {
	string hadankaData = File.ReadAllText(hadankaSoubor);
	Console.WriteLine(hadankaData);
	
	Console.Write("Co je to: ");
	string odpoved = Console.ReadLine();
	string odpovedData = File.ReadAllText(odpovedSoubor);
	if (odpovedData.Trim().ToLower() == odpoved.Trim().ToLower()) {
		Console.WriteLine("Správná odpověď.");
	}
	else {
		Console.WriteLine("Bohužel, špatně.");
	}
}
else {
	Console.WriteLine("Soubor: " + hadankaSoubor + " nenalezen");
}
