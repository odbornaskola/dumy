Console.WriteLine ("Program: Do školy v1.0");

Console.WriteLine("Na otázky odpovídej \"ano\" nebo \"ne\".");

string volba;

Console.Write("Je dnes všední den? ");
volba = Console.ReadLine();
if (volba == "ano") {
    Console.WriteLine("Jedu do školy.");
    Console.Write("Zaspal(a) jsem? ");
    volba = Console.ReadLine();
    if (volba == "ano") {
        Console.WriteLine ("Pojedu vlakem.");
    }
    else if (volba == "ne") {
        Console.WriteLine ("Pojedu autobusem.");
    }
    else {
        Console.WriteLine ("Neumím rozhodnout.");
    }

    Console.WriteLine ("Jsem ve škole.");
    Console.Write("Je konec školy? ");
    volba = Console.ReadLine();
    if (volba == "ano") {
        Console.WriteLine("Mám volno!");
    }
    else if (volba == "ne") {
        Console.WriteLine("Jsem stále vše škole!");
    }
    else {
        Console.WriteLine ("Nevím, co se děje.");
    }
}
else if (volba == "ne") {
    Console.WriteLine("Mám volno!");
}
else {
    Console.WriteLine("Neplatná volba!");
}
