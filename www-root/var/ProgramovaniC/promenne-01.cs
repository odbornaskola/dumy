using System;

namespace Promenne01
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            Console.WriteLine("Program: Proměnné a datové typy 1.0\n");

            // deklarace proměnné spočívá v zápisu datového typu a následně názvu proměnné
            // dále už se pracuje jen s názvem proměnné
            int    celeCislo;
            // názvy proměnných se mohou skládat jen z některých znaků, např. nesmí obsahovat mezeru
            int    cele_cislo;
            double desetCislo;
            bool   pravdaNepravda;
            string text;

            // pro zápis hodnot do proměnných se používá operátor "="
            celeCislo  = 155;
            cele_cislo = 42;
            // u desetinných čísel píšeme desetinnou tečku
            desetCislo = 3.14;
            // pravdivostní hodnota může být buď true a nebo false
            pravdaNepravda = true;

            // jakýkoli text se musí uzavřít do uvozovek
            text = "bflmpsvz";

            // výpis proměnných na obrazovku
            Console.WriteLine(celeCislo);
            // výpis lze rozšířit o další řetězec a tyto pak spojit pomocí "+"
            Console.WriteLine("Celé číslo: " + cele_cislo);
            Console.WriteLine("Desetinné číslo: " + desetCislo);
            Console.WriteLine("Boolean (pravda): " + pravdaNepravda);
            pravdaNepravda = false;
            Console.WriteLine("Boolean (nepravda): " + pravdaNepravda);
            Console.WriteLine("Text: " + text);

            Console.Write("Press any key to continue . . . ");
            Console.ReadKey(true);
        }
    }
}

