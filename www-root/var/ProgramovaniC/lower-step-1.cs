string answer;
Console.Write("Je již rozhodnuto? ");

answer = Console.ReadLine();

if (answer == "ano") {
    Console.WriteLine("Výborně, můžeme pokračovat");
}
else if (answer == "ne") {
    Console.WriteLine("A kde to vázne?");
}
else {
    Console.WriteLine("Chyba! Odpověď nebyla rozpoznána.");
}