string slovo;
string[] slovnik;

Console.WriteLine("Pro ukončení napiš \"!\".");

while (true) {
	Console.Write("Slovo: ");
	slovo = Console.ReadLine();
	
	if (slovo == "!") {
		Console.WriteLine("Končím!");
		break;
	}
	
	if (File.Exists("slovnik.txt") == true) {
		slovnik = File.ReadAllLines("slovnik.txt");
		
		bool nasel = false;
		for (int i = 0; i < slovnik.Length; i++) {
			if (slovnik[i] == slovo) {
				nasel = true;
				break;
			}
		}
		
		if (nasel == true) {
			Console.WriteLine("Toto slovo již znám.");
		}
		else {
			Console.WriteLine("Neznámé slovo.");
			File.AppendAllText("slovnik.txt", slovo + "\r\n");
			Console.WriteLine("Uloženo.");
		}
	}
	else {
		Console.WriteLine("Neznámé slovo");
		File.WriteAllText("slovnik.txt", slovo + "\r\n");
		Console.WriteLine("Uloženo.");
	}
}
