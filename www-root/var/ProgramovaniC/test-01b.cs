using System;

namespace TestB
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            Console.WriteLine ("Program: Upload v1.0\n");

            /**
             * Vytvořte program pro výpočet objemu odesílaných dat.
             *
             * Do proměnné uložte čas v hodinách (např. 1h)
             * Do jiné proměnné uložte rychlost odesílání v Mbit/s (např. 10 Mbit/s)
             *
             * Spočítejte objem přenesených dat za daný čas a při dané rychlosti.
             *   - převeďte čas na vteřiny (protože rychlost je v Mbit za vteřinu)
             *   - proveďte vynásobení rychlosti a času (zde 10 * 3600)
             *
             * Vypište výsledek na monitor.
             * Přepočítejte výsledek z Mbit na MB, platí že 1B = 8b, takže 36000/8.
             */


            // deklarace
            double objem;
            double objemBit;
            double rychlost;
            double casH;
            double cas;

            // přiřazení hodnot
            casH = 1; // čas v hodinách
            rychlost = 10; // rychlost v Mbit/s

            // operace
            cas = casH * 60 * 60;
            objemBit = cas * rychlost;
            objem = objemBit / 8;

            // výstup
            Console.WriteLine("Doba nahrávání   : " + casH + " h");
            Console.WriteLine("Převod h->s      : " + cas + " s");
            Console.WriteLine("Rychlost přenosu : " + rychlost + " Mbit/s");
            Console.WriteLine("-----------------------------");
            Console.WriteLine("Objem dat        : " + objemBit + " Mbit");
            Console.WriteLine("                   " + objem + " MB");
        }
    }
}
