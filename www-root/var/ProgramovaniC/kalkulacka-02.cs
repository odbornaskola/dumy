using System;

namespace Kalkulacka2
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            Console.WriteLine("Program: Kalkulačka v2.0\n");

            /**
             * Vytvořte program, který vyzve uživatele k zadání
             * dvou čísel a početní operace (+,-,*,/)
             * 
             * Následně program vyhodnotí pomocí switche jakou
             * operaci má vykonat a provede odpovídající výpočet.
             * 
             * Program zobrazí výsledek výpočtu na monitor.
             */         

            double x;
            double y;
            double vys;
            string operace;

            Console.Write("Zadej operaci (+,-,*,/): ");
            operace = Console.ReadLine();

            Console.Write("Zadej číslo: ");
            x = double.Parse(Console.ReadLine());

            Console.Write("Zadej druhé číslo: ");
            y = double.Parse(Console.ReadLine());

            switch (operace) {
            case "+":
                vys = x + y;
                Console.WriteLine(x + " + " + y + " = " + vys);
                break;
            case "-":
                vys = x - y;
                Console.WriteLine(x + " - " + y + " = " + vys);
                break;
            case "*":
                vys = x * y;
                Console.WriteLine(x + " * " + y + " = " + vys);
                break;
            case "/":
                vys = x / y;
                Console.WriteLine(x + " / " + y + " = " + vys);
                break;
            default:
                Console.WriteLine("Neznámý operátor!!!");
                break;
            }
            
            Console.Write("Press any key to continue . . . ");
            Console.ReadKey(true);
        }
    }
}
