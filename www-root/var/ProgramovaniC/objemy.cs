public static void Main (string[] args)
{

    Console.WriteLine("Program: Objemy v1.0\n");

    Console.WriteLine("Zvol objekt pro výpočet objemu:");
    Console.WriteLine("a) Krychle, b) Kvárdr, c) Koule");
    Console.Write (":");
    string volba = Console.ReadLine();
    if (volba == "a") {
        double strana;
        strana = ZadejCislo("Zadej stranu a: ");
        double objem;
        objem = Krychle(strana);
        Console.WriteLine("Objem krychle = " + objem + "j^3");
    }
    else if (volba == "b") {
        double stranaA = ZadejCislo("Zadej stranu a: ");
        double stranaB = ZadejCislo("Zadej stranu b: ");
        double stranaC = ZadejCislo("Zadej stranu c: ");
        
        double objem = Kvadr(stranaA, stranaB, stranaC);
        Console.WriteLine("Objem kvádru = " + objem + "j^3");
    }
    else if (volba == "c") {
        double objem = Koule(ZadejCislo("Zadej poloměr r: "));
        Console.WriteLine("Objem koule = " + objem + "j^3");
    }
    else {
        Console.WriteLine("Neznámá volba");
    }
    
    
    Console.Write("Press any key to continue . . . ");
    Console.ReadKey(true);

}

public static double ObjemKrychle(double a) {
    double objem;
    objem = a*a*a;

    return objem;
}

public static double ObjemKvadr(double a, double b, double c) {
    double objem;
    objem = a*b*c;

    return objem;
}

public static double ObjemKoule(double r) {
    double objem;
    objem = 4*Math.PI*r*r*r/3;

    return objem;
}

public static double ZadejCislo(string vyzva) {
    double cislo;
    Console.Write(vyzva);
    cislo = int.Parse(Console.ReadLine());

    return cislo;
}
