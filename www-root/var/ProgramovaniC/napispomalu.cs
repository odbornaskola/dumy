public static bool Otazka() {
    string klav;
    bool odpoved;

    while (true) {
        klav = Odpoved();
        if (klav == "ano") {
            odpoved = true;
            break;
        }
        else if (klav == "ne") {
            odpoved = false;
            break;
        }
        else {
            NapisPomalu("Nerozumím, zadej buď \"ano\" nebo \"ne\".");
        }
    }

    return odpoved;
}

public static void Pauza(int doba = 200) {
    Thread.Sleep(doba);
}

public static string Odpoved() {
    Console.Write(": ");
    return Console.ReadLine();
}

public static void NapisPomalu(string text) {
    int rychlost = 100;

    foreach (char znak in text.ToCharArray()) {
        if (rychlost > 50) {
            Thread.Sleep(rychlost);
        }
        Console.Write(znak);
    }
    Console.Write("\n");
}
