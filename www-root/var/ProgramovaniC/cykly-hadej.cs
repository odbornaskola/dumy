Console.WriteLine ("Hádej číslo");

Random nahoda = new Random();
int cislo = nahoda.Next(1, 100);

Console.WriteLine("Hádej číslo od 1 do 100");
int volba = 0;

while (volba != cislo) {
    Console.Write("Hádej číslo: ");
    int.TryParse(Console.ReadLine(), out volba);

    if (volba > cislo) {
        Console.WriteLine("Moje číslo je menší");
    }
    else if (volba < cislo) {
        Console.WriteLine("Moje číslo je větší");
    }

}

Console.WriteLine("Výborně, moje číslo bylo: " + cislo);
