using System;

namespace VstupniMetody
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            Console.WriteLine("Program: Vstup z klávesnice");

            string jmeno;

            Console.Write("Napiš tvé jméno: ");
            // metoda ReadLine() pozastaví běh programu a čeká, dokud uživatel nestiskne klávesu Enter
            // následně uloží to, co uživatel napsal do proměnné "jmeno"
            jmeno = Console.ReadLine();

            Console.WriteLine("Ahoj " + jmeno + "!");

            /**
             * Metoda Console.ReadLine() vrací vždy datový typ string.
             *
             * Následující příklad je tedy špatně
             *   double cislo;
             *   cislo = Console.ReadLine();
             *
             * protože "cislo" je jiného datového typu, než ten, který vrací ReadLine()
             *
             * Jenomže my mnohdy potřebujeme zadat z klávesnice číslo, abychom s ním pak mohli dále provádět matematické operace.
             * "Konverzi" řetězce na číslo může obstarat metoda double.Parse() nebo int.Parse().
             * Ta se snaží prohledat zadaný řetězec, a pokud v něm rozpozná číslo, vrátí jej na výstup.
             *
             * Pozn. Metoda Parse() může též "vyhodit (throw)" tzv. výjimku (exception).
             * To se může stát např. tehdy, zadáte-li takový řetězec, který neobsahuje číslo.
             * Prozatím nechme tento stav neošetřený, a vrátíme se k němu později.
             *
             */

            // zadávání čísel z klávesnice
            Console.Write("Zadej číslo : ");
            int cislo;
            // pokud chceme
            cislo = int.Parse(Console.ReadLine());
            Console.WriteLine("Vložené číslo: " + cislo);


            Console.Write("Press any key to continue . . . ");
            Console.ReadKey(true);
        }
    }
}
