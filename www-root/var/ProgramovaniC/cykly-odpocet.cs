int sec = 0;
Console.Write("Zadej počet vtřetin: ");
string hodnota = Console.ReadLine();
int.TryParse(hodnota, out sec);

while (sec >= 0) {
	Console.SetCursorPosition(0, 2);
	Console.Write(sec);
	Thread.Sleep(1000);
	sec--;

}
