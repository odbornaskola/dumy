<?php

/**
 * Umožňuje definovat vlastní nastavení, které přebije to defaultní. Tzn. že
 * žádná z níže uvedených vlastností není povinná.
 *
 * Vlastnosti se definují jako indexy v poli a toto pole má svůj index s názvem
 * třídy (včetně namespace), pro kterou jsou určeny.
 *
 * Např. Mějme třídu FileManager, která umožňuje nastavit různé velikosti
 * náhledů (small, medium a large). Tato třída se nachází v namespace \My\Tools.
 *
 * return array(
 *     '\My\Tools\FileManager' => array(
 *         'small'  => 320,
 *         'medium' => 512,
 *         'large'  => 916,
 *     ),
 * );
 *
 *
 * Tento soubor se inkluduje při inicializaci aplikace, není tedy možné hodnoty
 * dynamicky měnit za běhu aplikace
 *
 *
 * To, jaké vlastnosti umožňuje daná třída nastavit, je popsáno u každé třídy.
 */

return array(
    'Sh\Web\JScript' => array(
        'jquery' => true,
        'jquery-ui' => false,
    ),
    'Dumy\Dum' => array(
        'master' => 'pomykacz',
        'master_name' => 'Ivan Pomykacz',
        'kod_prefix' => 'VY_32_INOVACE_',
    ),
);