<?php

/**
 * Třída pro základní nastavení aplikace
 *
 * Nastavení lze měnit pouze přímou editací tohoto souboru, nikoli runtime.
 *
 */
class ConfigLocal {

    /**
     * Název aplikace (pro pojmenování je třeba použít stejná pravidla jako pro názvy proměnných)
     * $appname je prázdné, pokud se vyvíjí jádro MyApp
     *
     * @var string
     */
    public $appname = '';

    /**
     * Je toto produkční app?
     * @var bool
     */
    public $production = false;

    /**
     * Debug vystupy zapnuto/vypnuto
     * @var boolean
     */
    public $debug = true;

    /**
     * Debugovat i POST requesty
     * @var bool
     */
    public $debugPostRequest = false;

    /**
     * Specifikovat IP pro debugging, je-li uvedeno, přetíží hodnotu debug výše.
     * @var string
     */
    public $debugIP = '';

    /**
     * Povolit přístup do konfigurace z frontendu (http://web.domain/?layout=shell-config)
     * @var bool
     */
    public $configAccess = true;

    /**
     * Přetíží hodnotu _SERVER['SERVER_NAME']
     * @var string
     */
    public $server_name = '';

    /**
     * Přetíží hodnotu _SERVER['DOCUMENT_ROOT']
     * @var string
     */
    public $document_root = '';

    /**
     * Relativni cesta vůči kořenovému adresáři na souborovém systému
     * @var string
     */
    public $relroot = '';

    /**
     * Relativni cesta vůči document_root
     * @var string
     */
    public $relweb  = '';

    /**
     * Vynutí použití https při tvorbě url
     * Je zde kvůli proxy (nginx)
     * @var bool
     */
    public $forceSSL = false;

    /**
     * Pokud web používá SSO, zde se uvede hostname na sso server
     * @var string
     */
    public $sso_server = '';

    /**
     * Globální nastavení, zda-li se vůbec pokoušet o sestavení komunikace s DB.
     * @var boolean
     */
    public $usedb = false;

    /**
     * Povolení cachování
     * @var int
     */
    public $cache = false;

    /**
     * Používat relativní adresy na místo absolutních
     * @var boolean
     */
    public $relativ = false;

    /**
     * Globální locale pro aplikaci, lze přetížit v rámci instance App
     * @var string
     */
    public $locale = 'cs_CZ';

    /**
     * Globální jazyk pro aplikaci, lze přetížit v rámci instance App
     * @var string
     */
    public $language = 'cs';

    /**
     * Časová zóna
     * @var string
     */
    public $timezone = 'Europe/Prague';

    /**
     * Odesílat chybová hlášení
     * @var boolean
     */
    public $report = true;

    /**
     * Úroveň reportování 1-3 (1 - error, 2 - warning, 3 - notice)
     * @var int
     */
    public $reportSeverity = 1;

    /**
     * Adresát pro reporty
     * @var string
     */
    public $reportEmail = 'mist@a-las.net';

    /**
     * Identifikátor stránek pro webové statistiky
     * @var int
     */
    public $statSiteId = 0;

    /**
     * Název hosta, na kterém běží server pro zpracování webových statistik
     * @var string
     */
    public $statHostName = 'stat.a-las.net';

    /**
     * Autentizační token pro cron. Není-li vyžadován, nechat hodnotu prázdnou.
     * @var string
     */
    public $cronToken = 'bflmpsvz';

    /**
     * Povolí spuštění cronu pouze z dané sítě/ip adresy
     * @todo Není implementováno
     * @var string
     */
    public $cronAllow = '127.0.0.0/8';

    /**
     * Zobrazit výstup
     * @var boolean
     */
    public $flush = true;
}

