<?php
/**
 * User: mist
 * Date: 24.6.13
 * Time: 17:26
 */

namespace Dumy;


use \Sh\Ell\Shell,
    \Sh\App\C;

/**
 * Class Logo
 * @package Dumy
 */
class Logo extends \Sh\App\Web {
    public function set_assets() {

    }

    public function action() {

    }

    public function html() {
        $tpl = Shell::tpl();

        $tpl->assign(array(
            'base' => Shell::url()->build(),
            'up_switch' => Shell::url()->build(),
            'handler' => $this->handler(),
            'action' => 'switch',
            'switch_uc' => $this->sessionGet('switch') == 0 ? 'switch-emph' : '',
            'switch_zak' => $this->sessionGet('switch') == 1 ? 'switch-emph' : '',
            'switch_val' => $this->sessionGet('switch') == 1 ? '1' : '0',
        ));

        if (Shell::getThreadAssets('Dumy\Dum')->id > 0) {
            $tpl->parse('logo.switch');
        }

        $tpl->parse('logo');

        return $tpl->render();
    }

    public function post() {

    }

    public function ajax() {
        $this->trigger($this->assets->action);
    }

    protected function actionSwitch() {
        $sw = 0;

        if ($this->sessionGet('switch') == 0) {
            $sw = 1;
        }

        $this->sessionSet('switch', $sw);

        Shell::$http->sendJSON(array(
            'status' => '0',
            'switch' => $sw,
        ));
    }
}