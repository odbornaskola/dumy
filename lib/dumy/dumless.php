<?php

namespace Dumy;

use \Sh\Ell\Shell,
    \Sh\Tools\Utils,
    \Sh\App\C;
use Sh\Tools\Template;

/**
 * Class Dum
 * @package Dumy
 */
class DumLess extends Dum {

    public function html() {
        Shell::$html->css('fonts/serif/cmun-serif.css');
        Shell::$html->css('fonts/typewriter/cmun-typewriter.css');
        Shell::$html->css('fonts/bright/cmun-bright.css');
        Shell::$html->css('styly.css');
        Shell::$html->css('stylyless.css');
        Shell::$seo->title = 'DUMy | '.Shell::$cfg->retrieve('master_name', 'Dumy\Dum');

        $tpl = Shell::tpl('DumyDum');

        if (($data = $this->load($this->assets->id)) !== null) {

            Shell::$seo->title = $data->nazev . ' | ' . Shell::$seo->title;

            $tpl->assign(array(
                'title' => $data->skupina,
                'subtitle' => $data->nazev,
                'header' => '',
                'baseurl' => Shell::url()->hostOnly()->build(),
                'content' => $this->renderd($data),
                'toPDFsingle' => Shell::url()->setScript('var/pdf/' . $this->getPDFSafeName(self::PDF_PREFIX_DUM, $data->skupina, $data->nazev))->build(),
            ));

            $tpl->parse('dum');
        }

        return $tpl->render();
    }
}