<?php
/**
 * User: mist
 * Date: 6.6.13
 * Time: 22:01
 */

namespace Dumy;


use \Sh\Ell\Shell,
    \Sh\App\C;

/**
 * Class Menu
 * @package Dumy
 */
class Menu extends \Sh\App\Web {
    public function set_assets() {
        $a[] = $this->newAsset('value')
            ->setContext(C::T_GET)
            ->setDefault(0)
            ->setType(C::T_TYPE_INT);

        return $a;
    }

    public function action() {

    }

    public function html() {
        $tpl = Shell::tpl();

        $id = 0;
        if (($da = Shell::getThreadAssets('Dumy\Dum')) !== false) {
            $id = $da->id;
        }

        $tpl->assign(array(
            'struc' => $this->render(),
            'up_switch' => Shell::url()->build(),
            'handler' => addslashes($this),
            'action' => 'switch',
            'direct' => Shell::url()->setHandler('Dumy\\Dum')->setLayout('direct')->setAction('direct')->build(array('id' => $id)),
            'switch_uc' => $this->sessionGet('switch') == 0 ? 'switch-emph' : '',
            'switch_zak' => $this->sessionGet('switch') == 1 ? 'switch-emph' : '',
            'switch_val' => $this->sessionGet('switch') == 1 ? '1' : '0',
        ));

        if ($id > 0) {
            $tpl->parse('menu.buttons');
            $master = Shell::$cfg->retrieve('master', 'Dumy\Dum');
            $tpl->assign('moodle', '[DUMY]'.$master.'/'.$id.'[DUMY]');
            $tpl->parse('menu.linker');
            $tpl->parse('menu.cc');
        }

        $tpl->parse('menu');

        return $tpl->render();
    }

    protected function load() {
        $enabled = "WHERE dumy.enabled == 1";
        if (Shell::$isLocal) {
            $enabled = '';
        }

        $arch = 0;
        $archno = 0;

        if ($arch > 0) {
            if ($enabled) {
                $enabled.= " AND sady.rowid='" . $arch . "'";
            }
            else {
                $enabled = "WHERE sady.rowid='" . $arch . "'";
            }

            if ($archno > 0) {
                $enabled.= " AND dumy.cislo_archu='" . $archno . "'";
            }
        }

        $dumy = array();
        $virtuals = array();
        $res = Shell::$db->query("SELECT
                dumy.rowid id,
                dumy.nazev,
                dumy.virtual,
                dumy.enabled,
                skupiny.rowid id_skupiny,
                skupiny.nazev skupina,
                sady.nazev sada,
                virtual.id_parent id_parent
            FROM
                dumy
                JOIN skupiny ON (dumy.id_skupiny=skupiny.rowid)
                JOIN sady ON (dumy.id_sady=sady.rowid)
                LEFT JOIN virtual ON (dumy.rowid=virtual.id_dumy)
            ".$enabled."

            ORDER BY
                skupiny.poradi,
                dumy.poradi,
                virtual.poradi
        ");

        if ($res) {
            while (($data = $res->fetchArray(SQLITE3_ASSOC)) !== false) {
                if ($data['id_parent'] == 0) {
                    $sk = $data['skupina'];
                    if ($sk == 'null') {
                        $sk = $data['sada'];
                        $data['skupina'] = $sk;
                    }
                    $dumy[$data['skupina']][] = $data;
                }
                else {
                    $virtuals[$data['id_parent']][] = $data;
                }
            }
        }

        return array($dumy, $virtuals);
    }

    public function render() {
        $tp_skupiny = Shell::tpl('menu-struc');
        $tp_temata = Shell::tpl('menu-struc');
        $tp_virtuals = Shell::tpl('menu-struc');
        list($data, $virtuals) = $this->load();
        $id = null;
        $gid = null;
        if (($da = Shell::getThreadAssets('Dumy\Dum')) !== false) {
            $id = $da->id;
            $res = Shell::$db->query("SELECT s.rowid gid FROM dumy d INNER JOIN skupiny s ON (d.id_skupiny=s.rowid) WHERE d.rowid='".$id."'");
            if ($res) {
                if (($tmp = $res->fetchArray(SQLITE3_ASSOC)) !== false) {
                    $gid = $tmp['gid'];
                }
            }
        }
        foreach ($data as $skupina => $temata) {
            $hidden = 'hidden';
            $tp_skupiny->assign('nazev', $skupina);

            $tp_temata->reset();
            foreach ($temata as $tema) {
                $selected = '';

                if ($id > 0 and $tema['id'] == $id) {
                    $selected = 'menu-active';
                }

                $tp_temata->assign(array(
                    'nazev' => $tema['nazev'],
                    'url' => Shell::url()->setHandler('Dumy\\Dum')->build(array('id' => $tema['id'])),
                    'selected' => $selected,
                    'virtuals' => $this->addVirtuals($id, $tp_virtuals, $tema, $virtuals),
                ));

                if ($tema['enabled'] == 1) {
                    $tp_temata->parse('temata.item');
                }
                else {
                    $tp_temata->parse('temata.dis');
                }

                if ($tema['id_skupiny'] == $gid) {
                    $hidden = '';
                }
            }
            $tp_temata->parse('temata');
            $tp_skupiny->assign('temata', $tp_temata->render('temata'));
            $tp_skupiny->assign('hidden', $hidden);
            $tp_skupiny->parse('skupiny.item');
        }

        $tp_skupiny->parse('skupiny');
        return $tp_skupiny->render('skupiny');
    }

    /**
     * @param int $id
     * @param \Sh\Tools\Template $tpl
     * @param array $dum
     * @param array $virtuals
     * @return string
     */
    protected function addVirtuals($id, $tpl, $dum, $virtuals) {
        $out = '';

        if (array_key_exists($dum['id'], $virtuals)) {
            foreach ($virtuals[$dum['id']] as $d) {
                $selected = '';

                if ($d['id'] == $id) {
                    $selected = 'menu-active';
                }

                $tpl->assign(array(
                    'nazev' => $d['nazev'],
                    'url' => Shell::url()->setHandler('Dumy\\Dum')->build(array('id' => $d['id'])),
                    'selected' => $selected,
                ));

                if ($d['enabled'] == 1) {
                    $tpl->parse('virtuals.item');
                }
                else {
                    $tpl->parse('virtuals.dis');
                }
            }

            $tpl->parse('virtuals');
            $out = $tpl->render('virtuals');
            $tpl->reset();
        }

        return $out;
    }

    public function post() {

    }

    public function ajax() {

    }
}