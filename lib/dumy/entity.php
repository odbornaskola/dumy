<?php

namespace Dumy;

/**
 * Class Entity
 * @package Dumy
 * @property int $id;
 * @property int $id_skupiny
 * @property string $nazev;
 * @property int $poradi;
 * @property string $anotace;
 * @property string $autor;
 * @property string $jazyk;
 * @property string $vystup;
 * @property string $keywords;
 * @property string $vyukovy_zdroj;
 * @property string $vek;
 * @property string $interakce;
 * @property string $velikost;
 * @property string $kod;
 * @property string $rocnik;
 * @property string $specialni_potreby;
 * @property string $zhotoveno;
 * @property int $virtual;
 * @property string $sada;
 * @property string $skola;
 * @property string $adresa;
 * @property string $ic;
 * @property string $regno;
 * @property string $program;
 * @property string $skupina;
 * @property string $trida;
 * @property string $arch
 * @property string $cislo_archu
 */
class Entity extends \Sh\App\Component {
    protected $id;
    protected $id_skupiny;
    protected $nazev;
    protected $poradi;
    protected $anotace;
    protected $autor;
    protected $jazyk;
    protected $vystup;
    protected $keywords;
    protected $vyukovy_zdroj;
    protected $vek;
    protected $interakce;
    protected $velikost;
    protected $kod;
    protected $rocnik;
    protected $specialni_potreby;
    protected $zhotoveno;
    protected $virtual;
    protected $sada;
    protected $skola;
    protected $adresa;
    protected $ic;
    protected $regno;
    protected $program;
    protected $skupina;
    protected $trida;
    protected $arch;
    protected $cislo_archu;
}