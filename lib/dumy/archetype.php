<?php

namespace Dumy;


use \Sh\Ell\Shell,
    Sh\Tools\Utils,
    \Sh\App\C;

/**
 * Class Dum
 * @package Dumy
 */
abstract class Archetype extends \Sh\App\Web {
    /**
     * id aktuálně renderovaného DUMu nebo null
     * @var int|null
     */
    protected $id_dumy = null;

    /**
     * @var Assets
     */
    public $assets;

    const PDF_TYPE_DUM = 'dum';
    const PDF_TYPE_DUMY = 'dumy';
    const PDF_TYPE_SCHEDULE = 'schedule';
    const PDF_PREFIX_DUM = 'dum';
    const PDF_PREFIX_SHEET = 'sheet';
    const PDF_PREFIX_SCHEDULE = 'zaznamovy_arch';

    protected function getSkupiny() {
        $q = "SELECT rowid id FROM skupiny ORDER BY poradi";
        $data = Shell::$db->getRecordsSql($q);

        return $data;
    }

    /**
     * Načte DUM z databáze
     * @param $id
     * @return null|Entity
     */
    protected function load($id) {
        $data = null;
        $where = '';

        if ($id > 0) {
            $where = "";

            $res = Shell::$db->query("
                SELECT
                    dumy.rowid id,
                    dumy.nazev,
                    dumy.poradi,
                    dumy.trida,
                    dumy.anotace,
                    dumy.autor,
                    dumy.jazyk,
                    dumy.vystup,
                    dumy.keywords,
                    dumy.vyukovy_zdroj,
                    dumy.vek,
                    dumy.interakce,
                    dumy.velikost,
                    dumy.kod,
                    dumy.rocnik,
                    dumy.specialni_potreby,
                    dumy.zhotoveno,
                    dumy.virtual,
                    sady.nazev sada,
                    sady.rowid id_sada,
                    skoly.nazev skola,
                    skoly.adresa,
                    skoly.ic,
                    skoly.regno,
                    skoly.program,
                    skupiny.nazev skupina,
                    skupiny.rowid id_skupiny,
                    sady.kod arch,
                    dumy.cislo_archu
                FROM
                    dumy
                    JOIN skupiny ON (dumy.id_skupiny=skupiny.rowid)
                    JOIN sady ON (dumy.id_sady=sady.rowid)
                    JOIN skoly ON (dumy.id_skoly=skoly.rowid)
                WHERE
                    dumy.rowid='".$id."' AND
                    dumy.enabled='1'
                ORDER BY
                    skupiny.poradi,
                    dumy.poradi
            ");

            if ($res) {
                $data = $res->fetchArray(SQLITE3_ASSOC);
                if ($data['skupina'] == 'null') {
                    $data['skupina'] = $data['sada'];
                }

                if (empty($data)) {
                    $data = null;
                }
                else {
                    $data = new Entity($data);
                }
            }
        }

        return $data;
    }

    /**
     * @param int $id
     * @return Entity[]
     */
    protected function loadSkupina($id) {
        $out = array();

        if ($id > 0) {
            $q = "SELECT
                    dumy.rowid id,
                    dumy.nazev,
                    dumy.poradi,
                    dumy.trida,
                    dumy.anotace,
                    dumy.autor,
                    dumy.jazyk,
                    dumy.vystup,
                    dumy.keywords,
                    dumy.vyukovy_zdroj,
                    dumy.vek,
                    dumy.interakce,
                    dumy.velikost,
                    dumy.kod,
                    dumy.rocnik,
                    dumy.specialni_potreby,
                    dumy.zhotoveno,
                    dumy.virtual,
                    sady.nazev sada,
                    sady.rowid id_sada,
                    skoly.nazev skola,
                    skoly.adresa,
                    skoly.ic,
                    skoly.regno,
                    skoly.program,
                    skupiny.nazev skupina,
                    skupiny.rowid id_skupiny,
                    sady.kod arch,
                    dumy.cislo_archu
                    FROM
                        dumy
                        JOIN skupiny ON (dumy.id_skupiny=skupiny.rowid)
                        JOIN sady ON (dumy.id_sady=sady.rowid)
                        JOIN skoly ON (dumy.id_skoly=skoly.rowid)
                    WHERE
                        dumy.id_skupiny='".$id."' AND
                        dumy.rowid NOT IN (SELECT id_dumy FROM virtual) AND
                        dumy.enabled='1'
                    ORDER BY
                        dumy.poradi";

            $data = Shell::$db->getRecordsSql($q);
            foreach ($data as $d) {
                if ($d['skupina'] == 'null') {
                    $d['skupina'] = $d['sada'];
                }

                $out[] = new Entity($d);
            }
        }

        return $out;
    }

    /**
     * @param int $id
     * @param int $no
     * @return Entity[]
     */
    protected function loadArch($id, $no = 0) {
        $out = array();

        if ($id > 0) {
            $q = "SELECT
                    dumy.rowid id,
                    dumy.nazev,
                    dumy.poradi,
                    dumy.trida,
                    dumy.anotace,
                    dumy.autor,
                    dumy.jazyk,
                    dumy.vystup,
                    dumy.keywords,
                    dumy.vyukovy_zdroj,
                    dumy.vek,
                    dumy.interakce,
                    dumy.velikost,
                    dumy.kod,
                    dumy.rocnik,
                    dumy.specialni_potreby,
                    dumy.zhotoveno,
                    dumy.virtual,
                    sady.nazev sada,
                    sady.rowid id_sada,
                    skoly.nazev skola,
                    skoly.adresa,
                    skoly.ic,
                    skoly.regno,
                    skoly.program,
                    skupiny.nazev skupina,
                    skupiny.rowid id_skupiny,
                    sady.kod arch,
                    dumy.cislo_archu
                    FROM
                        dumy
                        JOIN skupiny ON (dumy.id_skupiny=skupiny.rowid)
                        JOIN sady ON (dumy.id_sady=sady.rowid)
                        JOIN skoly ON (dumy.id_skoly=skoly.rowid)
                    WHERE
                        dumy.id_sady='".$id."' AND
                        dumy.cislo_archu='".$no."' AND
                        dumy.rowid NOT IN (SELECT id_dumy FROM virtual) AND
                        dumy.enabled='1'
                    ORDER BY
                        dumy.poradi";

            $data = Shell::$db->getRecordsSql($q);
            foreach ($data as $d) {
                if ($d['skupina'] == 'null') {
                    $d['skupina'] = $d['sada'];
                }

                $out[] = new Entity($d);
            }
        }

        return $out;
    }

    /**
     * Vrátí finální HTML verzi DUMu
     * @param Entity $data řádek z tabulky dumy
     * @return string
     */
    protected function renderd(Entity $data) {
        $out = '';
        $tpl = Shell::tpl($this->getTemplateName($data));

        $skupina = $this->getSkupinaSafeName($data->skupina);
        $tema = $this->getTemaSafeName($data->nazev);

        $tpl->assign(array(
            'task_img' => Shell::url()->setScript('img/'.$skupina.'/'.$tema.'.png')->build(),
            'task_pdf' => Shell::url()->setScript('var/'.$skupina.'/'.$tema.'.pdf')->build(),
            'task_alt' => '',
            'task_src' => Shell::url()->setScript('var/'.$skupina.'/'.$tema.'.txt')->build(),
            'prefix' => Shell::url()->hostOnly()->build(),
            'skupina' => $data->skupina,
            'theme' => $skupina,
            'uloha' => $data->nazev,
            'task' => $tema,
            'nazev' => $data->nazev,
            'name' => $tema,
        ));

        $tpl->parse('dum');
        $out = $tpl->render();

        if ($data->virtual == 0) {
            list($toc, $content) = $this->toc($out, $data->id);
            $obsah = Shell::tpl('toc');
            $obsah->assign(array(
                'toc' => $toc,
            ));
            $obsah->parse('heading');
            $out = $obsah->render('heading') . $content;
        }

        return $out;
    }

    /**
     * @param Entity $data Dum
     * @return string
     */
    protected function getTemplateName($data) {
        $out = '';

        $out.= $this->getSkupinaSafeName($data->skupina);
        $out.= '/';
        $out.= $this->getTemaSafeName($data->nazev);

        return $out;
    }

    /**
     * @param string $text
     * @return mixed
     */
    protected function getSkupinaSafeName($text) {
        $out = Utils::safe_filename($text);
        $out = str_replace('-', ' ', $out);
        $out = ucwords($out);
        $out = str_replace(' ', '', $out);

        return $out;
    }

    /**
     * @param string $text
     * @return string
     */
    protected function getTemaSafeName($text) {
        return Utils::safe_filename($text);
    }

    /**
     * @param string $text
     * @return mixed
     */
    protected function getDumSafeName($text) {
        return $this->getSkupinaSafeName($text);
    }

    /**
     * @param $prefix
     * @param $skupina
     * @param string $nazev
     * @return string
     */
    protected function getPDFSafeName($prefix, $skupina, $nazev = '') {
        if ($prefix == self::PDF_PREFIX_DUM) {
            $prefix = '';
        }
        else {
            $prefix = $prefix . '_';
        }
        if ($nazev == '') {
            return $prefix . $this->getSkupinaSafeName($skupina) . '.pdf';
        }
        else {
            return $prefix . $this->getSkupinaSafeName($skupina) . '-' . $this->getDumSafeName($nazev) . '.pdf';
        }
    }

    /**
     * Generuje obsah DUMu, vrátí samostatný obsah a upravené tělo DUMu
     * @param string $content
     * @param int $dum_id
     * @return array
     */
    protected function toc($content, $dum_id) {
        $toc = '';
        $new_content = $content;
        if (preg_match_all('~(<h([0-9]).*?>(.*?)<\/h[0-9]>)~', $content, $matches, PREG_SET_ORDER) !== false) {
            //var_dump($matches);
            $toc_arr = array();
            $id = 0;
            foreach ($matches as $m) {
                $offset = 0;
                $all = false;
                while (($pos = mb_strpos($new_content, $m[1], $offset)) !== false and $all == false) {
                    $anchor = Utils::safe_filename(strip_tags($m[3]) . '.' . $dum_id . $offset);
                    $add = '<a name="'.$anchor.'"></a>';

                    if (mb_substr($new_content, $pos - mb_strlen($add), mb_strlen($add)) == $add) {
                        $offset = $pos + mb_strlen($m[1]) + mb_strlen($add);
                    }
                    else {
                        $new_content = mb_substr($new_content, 0, $pos) . $add . mb_substr($new_content, $pos, mb_strlen($new_content)-$pos);
                        $ti = new Toc();
                        $ti->id = $id;
                        $ti->anchor = $anchor;
                        $ti->label = $m[3];
                        $ti->level = $m[2];
                        $ti->is_parent = false;
                        $ti->parent_id = null;
                        $toc_arr[] = $ti;
                        $all = true;
                        $id++;
                    }
                }
            }

            //echo '<pre>'.print_r($this->analyzeTocParents2($toc_arr), true) . '</pre>';
            $toc = $this->generateToc($this->analyzeTocParents2($toc_arr));
            //$arr = $this->analyzeTocParents($toc_arr);
            //echo '<pre>'.htmlspecialchars($this->analyzeToc($arr), true) . '</pre>';

            //$toc = $this->generateToc($toc_arr);
        }

        return array($toc, $new_content);
    }

    /**
     * @param Toc[] $arr
     * @param int $parent_id
     * @return Toc[]
     */
    private function analyzeTocParents($arr, $parent_id = null) {
        if (count($arr) == 1) {
            $arr[0]->parent_id = $parent_id;
        }
        else {
            for ($t = 0; $t < count($arr); $t++) {
                if ($t + 1 < count($arr)) {
                    // zanoření, (čím větší level, tím větší zanoření), nekontroluje se posloupnost
                    if ($arr[$t+1]->level > $arr[$t]->level) {
                        $arr[$t]->is_parent = true;
                        $this->analyzeTocParents(array_slice($arr, $t+1), $arr[$t]->id);
                    }
                }

                if ($t - 1 >= 0 and $parent_id !== null) {
                    // vynoření, (čím menší level, tím více napovrch), posloupnost se kontroluje
                    if ($arr[$t-1]->level-1 == $arr[$t]->level) {
                        return $arr;
                    }
                }

                if ($parent_id !== null and $parent_id >= 0) {
                    $arr[$t]->parent_id = $parent_id;
                }
            }
        }

        return $arr;
    }

    /**
     * @param Toc[] $arr
     * @param int $parent_id
     * @return Toc[]
     */
    private function analyzeTocParents2($arr, $parent_id = null) {
        if (count($arr) > 0) {
            if (count($arr) == 1) {
                $arr[0]->parent_id = $parent_id;
            }
            else {
                $level = $arr[0]->level;
                $indexes = array();
                for ($t = 0; $t < count($arr); $t++) {
                    if ($arr[$t]->level == $level) {
                        $arr[$t]->parent_id = $parent_id;
                        $indexes[] = $t;
                    }
                }
                for ($t = 0; $t < count($indexes); $t++) {
                    if (isset($indexes[$t+1])) {
                        if ($indexes[$t]+1 != $indexes[$t+1]) {
                            $arr[$indexes[$t]]->is_parent = true;
                            $rest = array_slice($arr, $indexes[$t]+1, 1 + ($indexes[$t+1]-1) - ($indexes[$t]+1));
                            $this->analyzeTocParents2($rest, $arr[$indexes[$t]]->id);
                        }
                    }
                    else {
                        $rest = array_slice($arr, $indexes[$t]+1);
                        if (count($rest) > 0) {
                            $arr[$indexes[$t]]->is_parent = true;
                            $this->analyzeTocParents2($rest, $arr[$indexes[$t]]->id);
                        }
                    }
                }
            }
        }

        return $arr;
    }

    /**
     * @param Toc[] $arr
     * @return string
     */
    private function generateToc($arr) {
        $out = '';

        if (count($arr) > 0) {
            $tpl = Shell::tpl('toc');
            $parent_id = $arr[0]->parent_id;
            $i = -1;
            foreach ($arr as $t) {
                $i++;
                if ($parent_id !== $t->parent_id) {
                    continue;
                }
                else {
                    if ($t->is_parent) {
                        $subItems = $this->generateToc(array_slice($arr, $i+1));
                    }
                    else {
                        $subItems = '';
                    }

                    $tpl->assign(array(
                        'anchor' => $t->anchor,
                        'label' => $t->label,
                        'subItems' => $subItems,
                    ));

                    $tpl->parse('toc.item');
                }
            }

            $tpl->parse('toc');
            $out = $tpl->render('toc');
        }

        return $out;
    }

    public function cite($id) {
        $out = "<i>citation needed: $id</i>";

        $q = "SELECT rowid FROM biblio WHERE ref=:ref";
        $data = Shell::$db->getRecordsSql($q, array('ref' => $id));

        if (!empty($data)) {
            $out = '[<a class="citation" href="'.Shell::url()->setLayout('biblio')->build().'">' . $data[0]['rowid'] . '</a>]';
        }

        return $out;
    }

    /**
     * Vrátí HTML kód se zvýrazněnou syntaxí souboru
     * @param string $params
     * @return mixed|string
     */
    public function geshi($params) {
        list($filename, $lines, $high) = explode(',', $params);
        $filename = Shell::$cfg->root . '/var/'.$filename;

        if (($source = @file_get_contents($filename)) !== false) {
            require_once Shell::$cfg->root . '/../ext/geshi/geshi.php';
            $g = new \GeSHi($source, $this->getGeshiLanguage($filename));
            if ($lines) {
                $g->enable_line_numbers(\GESHI_NORMAL_LINE_NUMBERS);
            }
            if ($high) {
                $g->highlight_lines_extra(explode(':', $high));
            }
            $out = $g->parse_code();
        }
        else {
            $out = 'Error finding '.$filename;
        }

        return $out;
    }

    private function getGeshiLanguage($filename) {
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        switch ($ext) {
            case 'cs':
                $lang = 'csharp';
                break;
            case 'conf':
                $lang = 'apache';
                break;
            case 'phps':
                $lang = 'php';
                break;
            case 'html':
                $lang = 'html';
                break;
            default:
                $lang = $ext;
        }

        return $lang;
    }

    /**
     * Generuje referenční příručku pro podporovaný jazyk
     * @param string $id (csharp, excel)
     * @return string HTML výstup
     */
    public function ref($id) {
        $out = '';

        switch ($id) {
            case 'csharp':
                $out = $this->refCSharp();
                break;
            case 'excel':
                $out = 'excel';
                break;
            case 'linux':
                $out = $this->refLinux();
                break;
            default:
                $out = 'UNKNOWN REF';
                break;
        }

        return $out;
    }

    protected function refLinux() {
        $tpl = Shell::tpl('ref-linux');
        $groups = Shell::$dbms['ref.sqlite']->getRecordsSql('SELECT "rowid", "group" FROM linux_groups ORDER BY "order"');


        foreach ($groups as $group) {
            $data = Shell::$dbms['ref.sqlite']->getRecordsSql("SELECT * FROM linux  WHERE id_group='".$group['rowid']."' ORDER BY label");
            foreach ($data as $prikaz) {
                $tpl->assign($prikaz);
                $tpl->parse('ref.group.item');
            }
            $tpl->assign('group', $group['group']);
            $tpl->parse('ref.group');
        }

        $tpl->parse('ref');

        return $tpl->render();
    }

    protected  function refCSharp() {
        $tpl = Shell::tpl('ref-csharp');
        $mtd = clone $tpl;

        $data = Shell::$dbms['ref.sqlite']->getRecordsSql("SELECT * FROM csharp ORDER BY namespace, object, method");
        foreach ($data as $d) {
            // table
            $d['usage'] = $this->getCSharpUsage($d['usage']);
            $mtd->assign($d);

            if ($d['property'] == 0) {
                $mtd->assign('bracket', '()');
                $tpl->assign('bracket', '()');
            }
            else {
                $mtd->assign('bracket', '');
                $tpl->assign('bracket', '');
            }
            $mtd->parse('method');
            $tpl->assign(array(
                'method' => $mtd->render('method'),
                'heading' => $this->getCSharpMethod($d),
            ));
            $tpl->parse('ref.row');
            $mtd->reset();
        }

        $tpl->parse('ref');

        return $tpl->render('ref');
    }

    private function getCSharpMethod($data) {
        if ($data['namespace'] == 'builtin') {
            return implode('.', array($data['object'], $data['method']));
        }
        else {
            return implode('.', array($data['namespace'], $data['object'], $data['method']));
        }
    }

    private function getCSharpUsage($source) {
        require_once Shell::$cfg->root . '/../ext/geshi/geshi.php';
        $g = new \GeSHi($source, 'csharp');
        $out = $g->parse_code();

        return $out;
    }

    /**
     * Vrátí virtuální DUMy (tak jak je vrací $this->renderd())
     * @param string $id je buď identifikátor virtuálního DUMu a nebo konkrétního DUMu
     * @return string
     */
    public function virtual($id = null) {
        $load = array();
        // nechci ===, protože Template vrací prázdný řetězec
        if ($id == '') {
            $data = Shell::$db->getRecordsSql("SELECT id_dumy id FROM virtual WHERE id_parent='".$this->id_dumy."' ORDER BY poradi");
            foreach ($data as $d) {
                $load[] = $d['id'];
            }
        }
        else {
            $load[] = $id;
        }

        $tpl = Shell::tpl('virtual');
        foreach ($load as $l) {
            $dum = $this->load($l);
            if (!empty($dum)) {
                $tpl->assign(array(
                    'href' => Shell::url()->setHandler('Dumy\\Dum')->build(array('id' => $dum->id)),
                    'label' => $dum->nazev,
                ));
                $tpl->parse('virtual.item');
            }
        }

        $tpl->parse('virtual');

        return $tpl->render();
    }

    /**
     * Vrátí id rodiče, pokud jej najde
     * @param int $id
     * @return null|int
     */
    protected function findParent($id) {
        $out = null;
        $data = Shell::$db->getRecordsSql("SELECT id_parent id FROM virtual WHERE id_dumy='".$id."'");
        if (!empty($data)) {
            $out = $data[0]['id'];
        }

        return $out;
    }

    /**
     * Vrátí HTML odkaz na DUM
     * @param int $id
     * @return string
     */
    public function href($id) {
        if (($dum = $this->load($id)) != null) {
            return '<a href="'.Shell::url()->setHandler($this)->build(array('id' => $id)).'">'.$dum->nazev.'</a>';
        }
        else {
            return '<b>HREF ERROR ID!</b>';
        }
    }

}