<?php
/**
 * User: mist
 * Date: 6.6.13
 * Time: 21:22
 */

namespace Dumy;


use \Sh\Ell\Shell,
    \Sh\Tools\Utils,
    \Sh\App\C;
use Sh\Tools\Template;

/**
 * Class Dum
 * @package Dumy
 */
class Dum extends Archetype {

    public function set_assets() {

        $a[] = $this->newAsset('id')
            ->setContext(C::T_GET)
            ->setType(C::T_TYPE_INT)
            ->setDefault(0);

        $a[] = $this->newAsset('dum')
            ->setContext(C::T_GET)
            ->setDefault('pvy')
            ->setType(C::T_TYPE_STRING);

        $a[] = $this->newAsset('direct')
            ->setContext(C::T_GET)
            ->setDefault('0')
            ->setType(C::T_TYPE_STRING);

        $a[] = $this->newAsset('arch')
            ->setContext(C::T_GET)
            ->setType(C::T_TYPE_INT)
            ->setDefault(0);

        $a[] = $this->newAsset('archno')
            ->setContext(C::T_GET)
            ->setType(C::T_TYPE_INT)
            ->setDefault(0);

        $a[] = $this->newAsset('group')
            ->setContext(C::T_GET)
            ->setType(C::T_TYPE_INT)
            ->setDefault(0);

        return $a;
    }

    public function action() {
        $this->id_dumy = $this->assets->id;
        $this->trigger($this->assets->action);
    }

    protected function actionDirect() {
        $tpl = Shell::tpl();

        if (($data = $this->load($this->assets->id)) !== null) {
            $tpl->assign(array(
                'content' => $this->renderd($data),
            ));

            $tpl->parse('direct');
        }
        else {
            Shell::$http->sendNotFoundHeader();
            $tpl = Shell::tpl('not-found');
            $tpl->parse('not');
        }

        echo $tpl->render();

        $this->post = true;
        Shell::$flush = false;
    }

    public function html() {
        Shell::$html->css('fonts/serif/cmun-serif.css');
        Shell::$html->css('fonts/typewriter/cmun-typewriter.css');
        Shell::$html->css('fonts/bright/cmun-bright.css');
        Shell::$html->css('styly.css');
        Shell::$seo->title = 'DUMy | '.Shell::$cfg->retrieve('master_name');

        $tpl = Shell::tpl();

        if (($data = $this->load($this->assets->id)) !== null) {
            $h = new Hlavicka($data);

            if ($data->virtual == 0) {
                // pokud je DUM součástí virtuálu, pak dosadí hlavičku virtuálu
                if (($pid = $this->findParent($data->id)) !== null) {
                    $ndata = $this->load($pid);
                    $h = new Hlavicka($ndata);
                }
            }

            Shell::$seo->title = $data->nazev . ' | ' . Shell::$seo->title;

            $tpl->assign(array(
                'title' => $data->skupina,
                'subtitle' => $data->nazev,
                'header' => $h->render(),
                'baseurl' => Shell::url()->hostOnly()->build(),
                'content' => $this->renderd($data),
                'toPDFsingle' => Shell::url()->setScript('var/pdf/' . $this->getPDFSafeName(self::PDF_PREFIX_DUM, $data->skupina, $data->nazev))->build(),
            ));
            $tpl->parse('dum.pdflink');
            $tpl->parse('dum');
        }
        else {
            if ($this->assets->id > 0) {
                Shell::$http->sendNotFoundHeader();
                $tpl = Shell::tpl('not-found');
                $tpl->parse('not');
            }
            else {
                Shell::$html->inlineCss('#wrap { background-image: url(\''.Shell::url()->hostOnly()->build().'/img/logo-bw.png\'); background-repeat: no-repeat; background-position: bottom ; }');
                $tpl->assign(array(
                    'prefix' => Shell::url()->hostOnly()->build(),
                    'summary' => $this->summary(),
                    'master' => Shell::$cfg->retrieve('master'),
                    'master_name' => Shell::$cfg->retrieve('master_name'),
                ));
                $tpl->parse('intro');
            }
        }

        return $tpl->render();
    }

    protected function summary() {
        $tpl = Shell::tpl('dumy-summary');
        $tpl->assign('baseurl', Shell::url()->hostOnly()->build());
        $rows = array();

        $archsql = '';

        if ($this->assets->arch > 0) {
            $archsql = " AND sady.rowid='".$this->assets->arch."'";
            if ($this->assets->group > 0) {
                $tpl->assign('nadpis', 'Podrobný výpis');
                $archsql.= " AND skupiny.rowid='".$this->assets->group."'";
            }
            if ($this->assets->archno > 0) {
                $q = "SELECT sady.kod, dumy.cislo_archu cislo FROM dumy
                        JOIN sady ON (dumy.id_sady=sady.rowid)
                        WHERE dumy.id_sady='".$this->assets->arch."' AND dumy.cislo_archu='".$this->assets->archno."' AND dumy.kod!='' LIMIT 1";
                $res = Shell::$db->getRecordsSql($q);
                $tpl->assign('nadpis', $res[0]['kod'] . '/' . $res[0]['cislo']);
                $archsql.= " AND dumy.cislo_archu='".$this->assets->archno."'";
            }
        }
        else {
            // zobrazení seznamu dumů dle archů
            $tpl->assign('nadpis', 'Podrobný výpis');
            $tpl->parse('summary.info');
            $this->archy($tpl);
        }

        $res = Shell::$db->query("
            SELECT
                dumy.rowid,
                dumy.nazev,
                skupiny.nazev skupina,
                skupiny.rowid id_skupina,
                sady.nazev sada,
                dumy.anotace,
                dumy.kod,
                dumy.dokonceno,
                virtual.id_parent id_parent,
                sady.rowid arch,
                dumy.cislo_archu archno,
                skupiny.rowid arch_group
            FROM
                dumy
                JOIN skupiny ON (dumy.id_skupiny=skupiny.rowid)
                JOIN sady ON (dumy.id_sady=sady.rowid)
                LEFT JOIN virtual ON (dumy.rowid=virtual.id_dumy)
            WHERE
                enabled=1
                " . $archsql . "
            ORDER BY
                skupiny.poradi,
                dumy.poradi
            ");
        if ($res) {
            while (($data = $res->fetchArray(SQLITE3_ASSOC)) !== false) {
                if ($data['id_parent'] == 0) {
                    $sk = $data['skupina'];
                    if ($sk == 'null') {
                        $sk = $data['sada'];
                        $data['skupina'] = $sk;
                    }
                    $rows[$data['skupina']][] = $data;
                }
            }
        }

        foreach ($rows as $skupina => $row) {
            $prefix = Shell::$cfg->retrieve('kod_prefix');
            foreach ($row as $r) {
                if ($r['kod'] == '' or (Shell::$isLocal and $r['dokonceno'] < 100) ) {
                    $prefix = '<span class="progress-box"><span class="progress" style="background-color: '.$this->getProgressColor($r['dokonceno']).';width: '.$r['dokonceno'].'px;">'.$r['dokonceno'].'%</span></span>';
                }
                else {
                    $r['kod'] = str_replace($prefix, '', $r['kod']);
                }

                $tpl->assign($r);
                $tpl->assign(array(
                    'kod_prefix' => $prefix,
                    'url' => Shell::url()->setHandler('Dumy/Dum')->build(array('id' => $r['rowid'])),
                    'toPDFsingle' => Shell::url()->setScript('var/pdf/' . $this->getPDFSafeName(self::PDF_PREFIX_DUM, $skupina, $r['nazev']))->build(),
                    'toSheetSingle' => Shell::url()->setScript('var/pdf/' . $this->getPDFSafeName(self::PDF_PREFIX_SHEET, $skupina, $r['nazev']))->build(),
                ));

                $tpl->assign(array(
                    'skupina' => $skupina,
                    'toPDFall' => Shell::url()->setScript('var/pdf/' . $this->getPDFSafeName(self::PDF_PREFIX_DUM, $skupina))->build(),
                    'toSheetAll' => Shell::url()->setScript('var/pdf/' . $this->getPDFSafeName(self::PDF_PREFIX_SHEET, $skupina))->build(),
                ));

                $tpl->assign(array('skupina_link' => Shell::url()->setHandler($this)->build(array('arch' => $r['arch'], 'group' => $r['arch_group']))));

                $tpl->parse('summary.group.row');
            }



            $tpl->parse('summary.group');
        }

        $tpl->parse('summary');

        return $tpl->render();
    }

    private function archy(Template $tpl) {
        $q = "SELECT rowid archid FROM sady ORDER BY kod";
        $archy = Shell::$db->getRecordsSql($q);

        foreach ($archy as $arch) {
            $q = "SELECT dumy.id_sady arch, dumy.cislo_archu, sady.kod FROM dumy
                    INNER JOIN sady ON (dumy.id_sady=sady.rowid)
                    WHERE dumy.id_sady='".$arch['archid']."' AND dumy.cislo_archu > 0 AND dumy.cislo_archu != ''
                    GROUP BY dumy.cislo_archu
                    ORDER BY dumy.cislo_archu
            ";
            $subarchy = Shell::$db->getRecordsSql($q);
            foreach ($subarchy as $sub) {
                $q = "SELECT dumy.kod kod FROM dumy WHERE dumy.id_sady='".$sub['arch']."' AND dumy.cislo_archu='".$sub['cislo_archu']."' AND dumy.kod != '' ORDER BY dumy.kod ASC LIMIT 1";
                $res1 = Shell::$db->getRecordsSql($q);

                $q = "SELECT dumy.kod kod FROM dumy WHERE dumy.id_sady='".$sub['arch']."' AND dumy.cislo_archu='".$sub['cislo_archu']."' AND dumy.kod != '' ORDER BY dumy.kod DESC LIMIT 1";
                $res2 = Shell::$db->getRecordsSql($q);

                $sada = $res1[0]['kod'] . ' .. ' . $res2[0]['kod'];

                $tpl->assign(array(
                    'arch' => $sub['kod'],
                    'arch_url' => Shell::url()->setHandler($this)->build(array('arch' => $sub['arch'], 'archno' => $sub['cislo_archu'])),
                    'za_url' => Shell::url()->setLayout('layout-pdf')->setHandler('Dumy/PDF')->setAction('scheduleDownload')->build(array('id' => $sub['arch'], 'no' => $sub['cislo_archu'])),
                    'cislo' => $sub['cislo_archu'],
                    'sada' => $sada,
                ));

                $tpl->parse('summary.archy.item');
            }

        }

        $tpl->parse('summary.archy');
    }

    private function getProgressColor($progress) {
        if ($progress <= 20) {
            $out = 'red';
        }
        elseif ($progress <= 35) {
            $out = 'orangered';
        }
        elseif ($progress <= 50) {
            $out = 'lightcoral';
        }
        elseif ($progress <= 75) {
            $out = 'lightsalmon';
        }
        elseif ($progress <= 90) {
            $out = 'lightskyblue';
        }
        elseif ($progress < 100) {
            $out = 'lightgreen';
        }
        else {
            $out = 'green';
        }

        return $out;
    }

    public function post() {

    }

    public function ajax() {

    }

    /**
     * FIXME: neodpovídá aktuální db
     * @return bool
     */
    public function install() {
        Shell::$db->query("DROP TABLE IF EXISTS skoly");
        Shell::$db->query("CREATE TABLE skoly (
            nazev VARCHAR(64),
            adresa VARCHAR(128),
            ic int,
            regno VARCHAR(64),
            program varchar(64)
        )");

        Shell::$db->query("DROP TABLE IF EXISTS sady");
        Shell::$db->query("CREATE TABLE sady (
            nazev varchar(64) not null unique
        )");

        Shell::$db->query("DROP TABLE IF EXISTS skupiny");
        Shell::$db->query("CREATE TABLE skupiny (
            poradi int NOT NULL ,
            nazev varchar(64)
        )");

        Shell::$db->query("DROP TABLE IF EXISTS dumy");
        Shell::$db->query("CREATE TABLE dumy (
            id_skoly int,
            id_sady int,
            id_skupiny int not null,
            poradi int not null,
            enabled integer not null default 1,
            kod varchar(16) default '".Shell::$cfg->retrieve('kod_prefix')."',
            nazev varchar(64),
            anotace varchar(255),
            autor varchar(64) default '".Shell::$cfg->retrieve('master_name')."',
            jazyk varchar(64) default 'čeština',
            vystup varchar(255),
            keywords varchar(128),
            vyukovy_zdroj varchar(64),
            vek varchar(5),
            interakce varchar(64),
            velikost varchar(64),
            rocnik varchar(16),
            specialni_potreby varchar(128) default 'žádné',
            zhotoveno varchar(64)
        )");

        Shell::$db->query("DROP TABLE IF EXISTS virtual");
        Shell::$db->query('CREATE  TABLE "main"."virtual" ("id_parent" INTEGER NOT NULL , "id_dumy" INTEGER NOT NULL , "poradi" INTEGER, PRIMARY KEY ("id_parent", "id_dumy"))');

        // data
        Shell::$db->query("INSERT INTO skupiny (poradi, nazev) VALUES (-1, 'null')");
        Shell::$db->query("INSERT INTO skoly (nazev, adresa, ic, regno, program) VALUES ('Vyšší odborná škola obalové techniky a Střední škola, Štětí, příspěvková organizace',	'Kostelní 134, 411 08  Štětí',	46773509,	'CZ.1.07/1.5.00/34.1006',	'OP Vzdělávání pro konkurenceschopnost');");
        Shell::$db->query("INSERT INTO sady (nazev) VALUES ('Testovací sada')");
        Shell::$db->query("INSERT INTO dumy (id_skupiny, poradi, nazev, anotace, autor, jazyk, vystup, keywords, vyukovy_zdroj, vek, interakce, velikost, enabled, kod, id_sady, rocnik, specialni_potreby, zhotoveno, id_skoly) VALUES (1,	1,	'Dolor amet',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	1,	NULL,	'žádné',	NULL,	1);");

        return true;
    }
}