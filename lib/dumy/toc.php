<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mist
 * Date: 31.10.13
 * Time: 14:44
 * To change this template use File | Settings | File Templates.
 */

namespace Dumy;


use Sh\App\Component;

/**
 * Class Toc
 * @package Dumy
 * @property string $anchor
 * @property string $label
 * @property int $level
 * @property bool $is_parent
 * @property int $parent_id
 * @property int $id
 */
class Toc extends Component {
    protected $id;
    protected $label;
    protected $anchor;
    protected $level;
    protected $is_parent;
    protected $parent_id;

    function __toString() {
        $arr = array();
        foreach ($this->getProps() as $p) {
            $arr[$p] = $this->$p;
        }

        return '<pre>'.print_r($arr, true).'</pre>';
    }
}