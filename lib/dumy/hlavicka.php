<?php
/**
 * User: mist
 * Date: 6.6.13
 * Time: 21:30
 */

namespace Dumy;


use Sh\Ell\Shell;

class Hlavicka {
    /**
     * @var Entity
     */
    protected $dum;

    protected $opt;

    function __construct(Entity $dum, $opt = array()) {
        $this->dum = $dum;

        $params = array(
            'width' => 600,
            'width_lc1' => 150,
            'width_lc2' => '',
            'width_rc1' => 100,
            'width_rc2' => 100,
            'border' => 1,
        );

        foreach ($params as $p => $v) {
            if (array_key_exists($p, $opt)) {
                $this->opt[$p] = $opt[$p];
            }
            else {
                $this->opt[$p] = $v;
            }
        }
    }

    public function render() {
        $tpl = Shell::tpl('dum-header');
        $tpl->assign($this->opt);
        $tpl->assign($this->dum->toArray());
        $tpl->parse('header');

        return $tpl->render();
    }
}