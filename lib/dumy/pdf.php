<?php

namespace Dumy;

use Sh\App\C;
use \Sh\Ell\Shell;

class PDF extends Archetype {

    protected $book = array(
        'bookTitlePage',
        'bookLicencePage',
        'bookDisclaimerPage',
        'sectionTitlePage',
        'tocPage',
    );

    protected $dum = array(
        'contentHeaderPage',
        'sectionContent',
    );

    protected $other = array(
        'sheet',
        'schedule',
    );

    protected $pdfLoc = '';

    public function set_assets() {
        $a[] = $this->newAsset('id')
            ->setContext(C::T_GET)
            ->setType(C::T_TYPE_INT)
            ->setDefault(0);

        $a[] = $this->newAsset('type')
            ->setContext(C::T_GET)
            ->setType(C::T_TYPE_STRING)
            ->setDefault(null);

        $a[] = $this->newAsset('part')
            ->setContext(C::T_GET)
            ->setType(C::T_TYPE_STRING)
            ->setDefault('empty');

        $a[] = $this->newAsset('no')
            ->setContext(C::T_GET)
            ->setType(C::T_TYPE_INT)
            ->setDefault(0);

        return $a;
    }

    public function action() {
        $this->pdfLoc = Shell::$cfg->root . '/var/pdf';
        if (file_exists($this->pdfLoc)) {
            if (touch($this->pdfLoc . '/lock')) {
                unlink($this->pdfLoc . '/lock');
            }
            else {
                throw new \Exception('Nemohu zapsat do ' . $this->pdfLoc);
            }
        }
        else {
            throw new \Exception('Adresář neexistuje: ' . $this->pdfLoc);
        }

        if ($this->assets->part != 'empty') {
            if (!in_array($this->assets->part, $this->book) and
                !in_array($this->assets->part, $this->dum) and
                !in_array($this->assets->part, $this->other)) {
                throw new \Exception('Part: "' . $this->assets->part . '" does not exists.');
            }
        }

        $this->trigger($this->assets->action);
    }

    public function actionSheet() {
        Shell::$flush = false;

        if ($this->assets->id > 0 and $this->assets->type != null) {
            $filename = $this->getPDFileName($this->assets->id, $this->assets->type, self::PDF_PREFIX_SHEET);
            $pdf = $this->renderSheet($this->assets->id, $this->assets->type);
            $pdf->Output($filename, 'F');
        }
    }

    public function actionBook() {
        Shell::$flush = false;

        if ($this->assets->id > 0 and $this->assets->type != null) {
            $filename = $this->getPDFileName($this->assets->id, $this->assets->type, self::PDF_PREFIX_DUM);
            $pdf = $this->renderBook($this->assets->id, $this->assets->type);
            $pdf->Output($filename, 'F');
        }
    }

    public function actionSchedule() {
        Shell::$flush = false;

        if ($this->assets->id > 0) {
            $filename = $this->getPDFileName($this->assets->id, self::PDF_TYPE_SCHEDULE, self::PDF_PREFIX_SCHEDULE, $this->assets->no);
            $pdf = $this->renderSchedule($this->assets->id, $this->assets->no);
            $pdf->Output($filename, 'F');
        }
    }

    public function actionScheduleDownload() {
        if ($this->assets->id > 0) {
            $udmy = $this->loadArch($this->assets->id, $this->assets->no);
            $this->actionSchedule();
            $filename = $this->getPDFileName($this->assets->id, self::PDF_TYPE_SCHEDULE, self::PDF_PREFIX_SCHEDULE, $this->assets->no);
            Shell::$http->sendFileHeaders(pathinfo($filename, PATHINFO_BASENAME), 'application/pdf', filesize($filename));
            readfile($filename);
        }
    }

    public function actionCss() {
        Shell::$flush = false;
        Shell::$http->sendCssHeader();
        $filename = Shell::$cfg->root . '/html/css/'.$this->assets->part.'.css';
        if (file_exists($filename)) {
            readfile($filename);
        }
        else {
            throw new \Exception('CSS file not found!');
        }
    }

    public function actionHtml() {
        Shell::$flush = false;

        if ($this->assets->id > 0 and $this->assets->type == self::PDF_TYPE_DUM) {
            $parts = array_merge($this->book, $this->dum);
            $html = '';
            foreach ($parts as $p) {
                $html.= $this->getHtmlPage($this->assets->id, self::PDF_TYPE_DUM, $p);
            }
            echo hash('sha1', $html);
        }
    }

    /**
     * @param int $id
     * @param string $type
     * @param string $prefix
     * @param int $no
     * @return string
     * @throws \Exception
     */
    private function getPDFileName($id, $type, $prefix, $no = 0) {
        $out = '';
        $dum = null;

        switch ($type) {
            case self::PDF_TYPE_DUM:
                $dum = $this->load($id);
                $out = Shell::$cfg->root . '/var/pdf' . '/' . $this->getPDFSafeName($prefix, $dum->skupina, $dum->nazev);
                break;
            case self::PDF_TYPE_DUMY:
                $dumy = $this->loadSkupina($id);
                if (count($dumy) > 0) {
                    $dum = $dumy[0];
                    $out = Shell::$cfg->root . '/var/pdf' . '/' . $this->getPDFSafeName($prefix, $dum->skupina);
                }
                break;
            case self::PDF_TYPE_SCHEDULE:
                $dumy = $this->loadArch($id, $no);
                if (count($dumy) > 0) {

                    $dum = $dumy[0];
                    //FIXME: název souboru neodpovídá
                    $out = Shell::$cfg->root . '/var/pdf' . '/' . $this->getPDFSafeName($prefix, $dum->skupina);
                }
                break;
        }

        if ($out == '') {
            throw new \Exception('Nelze generovat nazev pdf souboru ('.$out.').');
        }

        return $out;
    }

    /**
     * @param int $id
     * @param string $type
     * @param string $part
     * @param int $no
     * @return string
     */
    protected function getHtmlPage($id, $type, $part, $no = 0) {
        $opt = array(
            'part' => $part,
            'type' => $type,
            'id' => $id,
            'no' => $no,
        );

        $url = Shell::url(true)->setLayout('layout-pdf')->setHandler($this)->build($opt);
        $raw = file_get_contents($url);

        return $raw !== false ? $raw : '';
    }

    /**
     * @param string $part
     * @return string
     */
    protected function getCssFile($part) {
        $opt = array(
            'part' => $part,
        );
        $url = Shell::url(true)->setLayout('layout-pdf')->setHandler($this)->setAction('css')->build($opt);
        $raw = file_get_contents($url);

        return $raw !== false ? $raw : '';
    }

    /**
     * @param int $id
     * @param string $type
     * @return \mPDF
     */
    protected function renderBook($id, $type) {
        require_once Shell::$cfg->root . '/../ext/mpdf/mpdf.php';

        $mpdf = new \mPDF('cs', 'A4');

        $this->addToPdf($mpdf, $id, $type, $this->book);

        switch ($type) {
            case self::PDF_TYPE_DUM:
                $opt = array(
                    'skupina' => '',
                    'dum' => '',
                );
                if (($dum = $this->load($id)) !== null) {
                    $opt['skupina'] = $dum->skupina;
                    $opt['dum'] = $dum->nazev;
                }

                $this->addToPdf($mpdf, $id, self::PDF_TYPE_DUM, $this->dum, array('header' => $opt, 'footer' => true), true);
                break;
            case self::PDF_TYPE_DUMY:
                $dumy = $this->loadSkupina($id);
                $i = 0;
                foreach ($dumy as $d) {
                    $last = false;
                    if (($i+1) == count($dumy)) {
                        $last = true;
                    }
                    $opt = array(
                        'skupina' => $d->skupina,
                        'dum' => $d->nazev,
                    );
                    $this->addToPdf($mpdf, $d->id, self::PDF_TYPE_DUM, $this->dum, array('header' => $opt, 'footer' => true), $last);
                    $i++;
                }
                break;
        }

        return $mpdf;
    }

    /**
     * @param int $id
     * @param string $type
     * @return \mPDF
     */
    protected function renderSheet($id, $type) {
        require_once Shell::$cfg->root . '/../ext/mpdf/mpdf.php';

        $mpdf = new \mPDF('cs', 'A4-L', 12, 'Helvetica', 0, 0, 5, 0, 0, 0);

        switch ($type) {
            case self::PDF_TYPE_DUM:
                $this->addToPdf($mpdf, $id, self::PDF_TYPE_DUM, array('sheet'), array(), true);
                break;
            case self::PDF_TYPE_DUMY:
                $dumy = $this->loadSkupina($id);
                $i = 0;
                foreach ($dumy as $d) {
                    $last = false;
                    if (($i+1) == count($dumy)) {
                        $last = true;
                    }
                    $this->addToPdf($mpdf, $d->id, self::PDF_TYPE_DUM, array('sheet'), array(), $last);
                    $i++;
                }
                break;
        }

        return $mpdf;
    }

    protected function renderSchedule($id, $no) {
        require_once Shell::$cfg->root . '/../ext/mpdf/mpdf.php';

        $mpdf = new \mPDF('cs', 'A4-L');

        $this->addToPdf($mpdf, $id, self::PDF_TYPE_SCHEDULE, array(self::PDF_TYPE_SCHEDULE), array('footer' => true), true, $no);

        return $mpdf;
    }

    /**
     * @param \mPDF $pdf
     * @param int $id
     * @param string $type
     * @param array $parts
     * @param array $opt
     * @param bool $last
     * @param int $no
     */
    private function addToPdf($pdf, $id, $type, $parts, $opt = array(), $last = false, $no = 0) {
        foreach ($parts as $part) {
            $html = $this->getHtmlPage($id, $type, $part, $no);
            $css = $this->getCssFile($part);
            if (array_key_exists('header', $opt) and $opt['header'] == true) {
                $header = array (
                    'odd' => array (
                        'L' => array (
                            'content' => $opt['header']['skupina'],
                            'font-size' => 10,
                            'font-style' => '',
                            'font-family' => 'serif',
                            'color'=>'#999999'
                        ),
                        'C' => array (
                            'content' => '',
                            'font-size' => 10,
                            'font-style' => '',
                            'font-family' => 'serif',
                            'color'=>'#c0c0c0'
                        ),
                        'R' => array (
                            'content' => $opt['header']['dum'],
                            'font-size' => 10,
                            'font-style' => '',
                            'font-family' => 'serif',
                            'color'=>'#999999'
                        ),
                        'line' => 1,
                    ),
                    'even' => array ()
                );
                $pdf->SetHeader($header, '', true);
            }
            if (array_key_exists('footer', $opt) and $opt['footer'] == true) {
                $footer = array(
                    'odd' => array (
                        'L' => array (
                            'content' => '',
                            'font-size' => 10,
                            'font-style' => 'B',
                            'font-family' => 'serif',
                            'color'=>'#000000'
                        ),
                        'C' => array (
                            'content' => '',
                            'font-size' => 10,
                            'font-style' => 'B',
                            'font-family' => 'serif',
                            'color'=>'#000000'
                        ),
                        'R' => array (
                            'content' => '{PAGENO}',
                            'font-size' => 10,
                            'font-style' => '',
                            'font-family' => 'serif',
                            'color'=>'#000000'
                        ),
                        'line' => 1,
                    ),
                    'even' => array (),
                );
                $pdf->SetFooter($footer);
            }
            $pdf->WriteHTML($css, 1);
            $pdf->WriteHTML($html, 2);
            if (!$last) {
                $pdf->AddPage();
            }
        }
    }

    /**
     * Titulka celého kompletu
     * @param int $id
     * @param string $type
     * @return string
     */
    private function bookTitlePage($id, $type) {
        Shell::$html->css('bookTitlePage.css');

        $tpl = Shell::tpl('book/book-title');

        $title = array(
            'title' => 'NO TITLE',
            'subtitle' => 'NO SUBTITLE',
        );

        switch ($type) {
            case self::PDF_TYPE_DUM:
                if (($dum = $this->load($id)) !== null) {
                    $title['title'] = $dum->sada;
                    $title['subtitle'] = $dum->skupina;
                }
                break;
            case self::PDF_TYPE_DUMY:
                $dumy = $this->loadSkupina($id);
                if (count($dumy) > 0) {
                    $title['title'] = $dumy[0]->sada;
                    $title['subtitle'] = $dumy[0]->skupina;
                }
                break;
        }

        $tpl->assign($title);
        $tpl->assign($this->basicIncludes());
        $tpl->parse('page');

        return $tpl->render('page');
    }

    /**
     * Text s licencí
     * @return string
     */
    private function bookLicencePage() {
        Shell::$html->css('bookLicencePage.css');

        $tpl = Shell::tpl('book/licence');

        $tpl->assign($this->basicIncludes());
        $tpl->parse('page');

        return $tpl->render('page');
    }

    /**
     * @return string
     */
    private function bookDisclaimerPage() {
        Shell::$html->css('bookDisclaimerPage.css');
        $tpl = Shell::tpl('book/disclaimer');

        $tpl->assign($this->basicIncludes());
        $tpl->parse('page');

        return $tpl->render('page');
    }

    /**
     * Úvodní strana s názvem skupiny
     * @param int $id
     * @return string
     */
    private function sectionTitlePage($id) {
        Shell::$html->css('sectionTitlePage.css');

        $tpl = Shell::tpl('book/section-title');

        if (($dum = $this->load($id)) !== null) {
            $tpl->assign(array(
                'sada' => $dum->sada,
                'skupina' => $dum->skupina,
            ));

        }

        $tpl->assign($this->basicIncludes());
        $tpl->parse('page');

        return $tpl->render('page');
    }

    /**
     * Obsah kompletu, tj. seznam DUMů
     * @param int $id
     * @param string $type
     * @return string
     */
    private function tocPage($id, $type) {
        Shell::$html->css('tocPage.css');
        $tpl = Shell::tpl('book/toc');

        $toc = array();

        switch ($type) {
            case self::PDF_TYPE_DUM:
                if (($dum = $this->load($id)) !== null) {
                    $toc[] = $dum->nazev;
                }
                break;
            case self::PDF_TYPE_DUMY:
                $dumy = $this->loadSkupina($id);
                foreach ($dumy as $d) {
                    $toc[] = $d->nazev;
                }
                break;
        }

        foreach ($toc as $t) {
            $tpl->assign('label', $t);
            $tpl->parse('page.row');
        }

        $tpl->assign($this->basicIncludes());
        $tpl->parse('page');

        return $tpl->render('page');
    }

    /**
     * Úvodní stránka k jednotlivým DUMům
     * @param int $id
     * @return string
     */
    private function contentHeaderPage($id) {
        Shell::$html->css('contentHeaderPage.css');
        $tpl = Shell::tpl('book/content-header');

        if (($dum = $this->load($id)) !== null) {
            $opt = array(
                'width' => 800,
                'width_lc1' => 300,
                'width_lc2' => '',
                'width_rc1' => 180,
                'width_rc2' => 100,
            );
            $h = new Hlavicka($dum, $opt);
            $tpl->assign(
                array(
                    'header' => $h->render(),
                    'skupina' => $dum->skupina,
                    'nazev' => $dum->nazev,
                ));
        }

        $tpl->assign($this->basicIncludes());
        $tpl->parse('page');

        return $tpl->render('page');
    }

    /**
     * DUM
     * @param int $id
     * @return string
     */
    private function sectionContent($id) {
        Shell::$html->css('sectionContent.css');
        $tpl = Shell::tpl('book/content');

        if (($dum = $this->load($id)) !== null) {
            $this->id_dumy = $id;
            $tpl->assign('content', $this->renderd($dum));
        }

        $tpl->assign($this->basicIncludes());
        $tpl->parse('page');

        return $tpl->render('page');
    }

    /**
     * @param int $id
     * @return string
     */
    private function sheet($id) {
        Shell::$html->css('sheet.css');
        $tpl = Shell::tpl('book/sheet');

        if (($dum = $this->load($id)) !== null) {
            $opt = array(
                'width' => 1000,
                'width_lc1' => 300,
                'width_lc2' => '',
                'width_rc1' => 200,
                'width_rc2' => 100,
                'border' => 0,
            );
            $h = new Hlavicka($dum, $opt);
            $tpl->assign('sheet', $h->render());
        }

        $tpl->assign($this->basicIncludes());
        $tpl->parse('page');

        return $tpl->render('page');
    }

    private function schedule($id, $no = 0) {
        Shell::$html->css('schedule.css');
        $tpl = Shell::tpl('book/schedule');

        $dumy = $this->loadArch($id, $no);
        if (count($dumy) > 0) {

            $dum = $dumy[0];
            $tpl->assign($dum->toArray());
            $tpl->parse('page.aheader');

            foreach ($dumy as $d) {
                $tpl->assign($d->toArray());
                $tpl->parse('page.arch.item');
            }
            $tpl->parse('page.arch');
        }

        $tpl->assign($this->basicIncludes());
        $tpl->parse('page');

        return $tpl->render('page');
    }

    /**
     * @return array
     */
    private function basicIncludes() {
        return array(
            'url' => Shell::url()->hostOnly()->build(),
            'master' => Shell::$cfg->retrieve('master', 'Dumy\Dum'),
            'master_name' => Shell::$cfg->retrieve('master_name', 'Dumy\Dum'),
        );
    }

    public function html() {
        $out = '';

        switch ($this->assets->part) {
            case 'bookTitlePage':
                $out = $this->bookTitlePage($this->assets->id, $this->assets->type);
                break;
            case 'bookLicencePage':
                $out = $this->bookLicencePage();
                break;
            case 'bookDisclaimerPage':
                $out = $this->bookDisclaimerPage();
                break;
            case 'sectionTitlePage':
                $out = $this->sectionTitlePage($this->assets->id);
                break;
            case 'tocPage':
                $out = $this->tocPage($this->assets->id, $this->assets->type);
                break;
            case 'contentHeaderPage':
                $out = $this->contentHeaderPage($this->assets->id);
                break;
            case 'sectionContent':
                $out = $this->sectionContent($this->assets->id);
                break;
            case 'sheet':
                $out = $this->sheet($this->assets->id);
                break;
            case 'schedule':
                $out = $this->schedule($this->assets->id, $this->assets->no);
                break;
            case 'empty':
                $out = 'empty page';
                break;
        }

        return $out;
    }

    public function virtual($id = null) {
        $load = array();
        // nechci ===, protože Template vrací prázdný řetězec
        if ($id == '') {
            $data = Shell::$db->getRecordsSql("SELECT id_dumy id FROM virtual WHERE id_parent='".$this->id_dumy."' ORDER BY poradi");
            foreach ($data as $d) {
                $load[] = $d['id'];
            }
        }
        else {
            $load[] = $id;
        }

        $tpl = Shell::tpl('book/virtual-pdf');
        foreach ($load as $l) {
            if (($dum = $this->load($l)) !== null) {
                $tpl->assign(array(
                    'nazev' => $dum->nazev,
                    'content' => $this->renderd($dum),
                ));
                $tpl->parse('virtual.item');
            }
        }

        $tpl->parse('virtual');

        return $tpl->render();
    }

    public function post() {

    }

    public function ajax() {

    }


}
