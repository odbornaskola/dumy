<?php
/**
 * User: mist
 * Date: 10.11.13
 * Time: 20:34
 */

namespace Dumy;

/**
 * Class Assets
 * @package Dumy
 * @property int $id
 * @property string $part
 * @property string $type
 * @property string $dum
 * @property string $direct
 * @property int $arch
 * @property int $archno
 * @property int $group
 */
class Assets extends \Sh\Web\Assets {

}