<?php
/**
 * User: mist
 * Date: 10.11.13
 * Time: 16:22
 */

namespace Dumy;


use \Sh\Ell\Shell,
    \Sh\App\C;

/**
 * Class Autogen
 * @package Dumy
 */
class Autogen extends Archetype {
    public function set_assets() {

    }

    public function action() {
        $skupiny = $this->getSkupiny();

        foreach ($skupiny as $sk) {
            $opt = array(
                'id' => $sk['id'],
                'type' => self::PDF_TYPE_DUMY
            );
            $this->request($opt);

            // individuální pdf
            $dumy = $this->loadSkupina($sk['id']);
            foreach ($dumy as $dum) {
                $opt = array(
                    'id' => $dum->id,
                    'type' => self::PDF_TYPE_DUM
                );
                $this->request($opt);
            }
        }
    }

    public function html() {

    }

    private function request($opt) {

        $urls = array(
            Shell::url(true)->setHandler('Dumy/PDF')->setLayout('layout-pdf')->setAction('book')->build($opt),
            Shell::url(true)->setHandler('Dumy/PDF')->setLayout('layout-pdf')->setAction('sheet')->build($opt)
        );

        if ($opt['type'] == self::PDF_TYPE_DUMY) {
            unset($opt['type']);
            $urls[] = Shell::url(true)->setHandler('Dumy/PDF')->setLayout('layout-pdf')->setAction('schedule')->build($opt);
        }

        foreach ($urls as $u) {

            echo $u;
            echo '<br />';
            echo $this->curl($u);
            echo '<br />';
        }
    }

    private function curl($url) {
        $out = file_get_contents($url);

        return $out;
    }

    public function post() {

    }

    public function ajax() {

    }
}