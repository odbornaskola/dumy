<?php
/**
 * User: mist
 * Date: 29.7.14
 * Time: 7:23
 */

namespace Dumy;


use \Sh\Ell\Shell,
    \Sh\App\C;

/**
 * Class Install
 * @package Dumy
 */
class Install extends \Sh\App\Web {
    public function set_assets() {

    }

    public function action() {

    }

    public function html() {

    }

    public function post() {

    }

    public function ajax() {

    }

    public function install() {
        Shell::$cfg->store('jquery', 'true', '', 'Sh\Web\JScript');
        Shell::$cfg->store('master', 'pomykacz', '', 'Dumy\Dum');
        Shell::$cfg->store('master_name', 'Ivan Pomykacz', '', 'Dumy\Dum');
        Shell::$cfg->store('kod_prefix', 'VY_32_INOVACE_', '', 'Dumy\Dum');

        return true;
    }
}